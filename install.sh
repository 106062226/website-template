# /bin/sh
mkdir build
cd build
mkdir dashboard
cd ..

# Build Dashboard
cd dashboard
npm run build
mv dist/* ../build/dashboard
rm -rf dist

cd ../backend

# Build Go server
make

mv server ../build
cp -r script ../build
cp key.env ../build
