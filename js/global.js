function backToTop() {
  scrollTo(0, 0);
}

function Goto(href) {
  console.log('href', href);
  window.location = href;
}

async function fetchDefaultInfoData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/defaultInfo/getAll")
  let rawData = await raw.json()
  // let rawData = [{ title: 'a', contact: 'b', link: 'd'}];
  let data = []

  if (rawData) {
    for (item of rawData) {
      data.push(item)
    }
  }

  console.log('default0: ', data);
  const ele1 = document.getElementsByClassName('p-title');
  for (e of ele1) {
    e.innerHTML = data[0].title;
  }
  // const ele2 = document.getElementsByClassName('page-logo');
  // console.log('ele2', ele2);
  // for (e of ele2) {
  //   e.src = data[0].link;
  // }
  const ele3 = document.getElementsByClassName('footer');
  for (e of ele3) {
    e.innerHTML = data[0].contact;
  }
}

fetchDefaultInfoData();