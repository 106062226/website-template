const docHtml = '<div class="doc list" onclick="Goto(\'$href\')"><div class="title"><img class="file-icon" src="assets/report.svg">$title</div></div>';
const searchHtml = '<div class="search list"><div class="title"><img class="file-icon" src="assets/link.svg"><a href="$url" target="_blank">$title</a></div></div>';

let docsData = [];

let searchData = []

function ScrollTo(idx) {
  if (screen.width >= 768) {
    document.getElementsByClassName('section')[idx].scrollIntoView();
  } else {
    selectTab(idx);
  }
}

function noScroll() {
  window.scrollTo(0,0)
}

function toggleMenu() {
  document.getElementById('expand-menu').classList.toggle('expand');
  document.getElementById('blur-cover').classList.toggle('expand');
  document.getElementById('body').classList.toggle('fixed');
}

async function fetchDocsData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/file/getAll")
  let rawData = await raw.json()
  let data = []

  if (rawData) {
    for (item of rawData) {
      console.log(item.link)
      data.push({
        title: item.name,
        url: `http://staff140.et.nthu.edu.tw:9000/${item.link}`
      })
    }
  }

  return data
}

async function fetchSearchLinksData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/onlineLink/getAll")
  let rawData = await raw.json()
  let data = []

  if (rawData) {
    for (item of rawData) {
      data.push({
        title: item.title,
        url: item.link
      })
    }
  }

  return data
}

function setDocs() {
  const doc = document.getElementById('doc');
  console.log(doc);
  console.log('docsData', docsData);
  doc.innerHTML = docsData.map((e, idx) => docHtml.replace('$title', e.title).replace('$href', e.url)).join('');
}
function setSearchs() {
  const search = document.getElementById('search');
  console.log(search);
  search.innerHTML = searchData.map(e => searchHtml.replace('$title', e.title).replace('$url', e.url)).join('');
}

let type = 0;
const data = [[],[]], title = ['111專書工作圈文件下載', '線上資料庫檢索'];

function setMobile() {
  data[0] = docsData.map((e, idx) => docHtml.replace('$title', e.title).replace('$href', e.url)).join('');
  data[1] = searchData.map(e => searchHtml.replace('$title', e.title).replace('$url', e.url)).join('');
}

function selectTab(idx) {
  document.getElementsByClassName('icon-block')[type].classList.remove('selected');
  type = idx;
  document.getElementsByClassName('icon-block')[type].classList.add('selected');

  const mobile = document.getElementById('mobile-section');
  mobile.innerHTML = data[type];
  document.getElementById('mobile-title').textContent = title[type];
}

async function initial() {
  docsData = await fetchDocsData()
  searchData = await fetchSearchLinksData()

  if (screen.width >= 768) {
    setDocs();
    setSearchs();
  } else {
    setMobile();
    selectTab(0);
  }
  console.log(screen.width);
}