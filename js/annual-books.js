const annualHtmlArr = [
  '<div id="$id" class="section">',
    '<div class="section-title">',
      '<span id="mobile-title" class="title-tab">$books-title</span>',
    '</div>',
    '<div class="section-content">',
      '$book-data',
    '</div>',
  '</div>'
];
const annualHtml = annualHtmlArr.join('');

const bookHtmlArr = [
  '<div class="sub-section">',
    '<div class="huge-circle">',
      '<img src="$image-url" alt="">',
      '<div class="fake-cover"></div>',
    '</div>',
    '<div class="info">',
      '<div class="info-section">',
        '<span class="title">書目</span>',
        '<span>$title</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">作者</span>',
        '<span>$author</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">出版社</span>',
        '<span>$public</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">相關介紹</span>',
      '</div>',
      '<div class="intro-section">',
        '$introduction',
      '</div>',
    '</div>',
  '</div>'
];
const bookHtml = bookHtmlArr.join('');

let data = [];

async function fetchData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/book/getAll")
  let rawData = await raw.json()
  let data = []

  if (rawData) {
    for (item of rawData) {
      let idx = data.findIndex(element => element.title === item.type)
      if (idx >= 0) {
        data[idx].data.push(item)
      } else {
        data.push({
          title: item.type,
          data: [
            item
          ]
        })
      }
    }
  }

  return data
}

function toggleMenu() {
  document.getElementById('expand-menu').classList.toggle('expand');
  document.getElementById('blur-cover').classList.toggle('expand');
  document.getElementById('body').classList.toggle('fixed');
}

function setAnnual() {
  const body = document.getElementById('body');
  const newHtml = data.map(d => {
    const books = d.data.map(dd => {
      return bookHtml
        .replace('$title', dd.name)
        .replace('$image-url', dd.image_link)
        .replace('$author', dd.author)
        .replace('$public', dd.publication)
        .replace('$introduction', dd.intro.replaceAll('\n', '<br>'))
    });
    return annualHtml
      .replace('$id', `${d.title_en}-section`)
      .replace('$books-title', d.title)
      .replace('$book-data', books)
  });
  body.innerHTML += newHtml;
}

function setPosition() {
  var x = window.location.href.indexOf('?');
  var query = x >= 0 ? window.location.href.substring(x + 1) : null;
  if (query && query.split('=')[0] === 'id') {
    const id = query.split('=')[1];
    document.getElementById('recommendation-section').getElementsByClassName('sub-section')[id].scrollIntoView();
  }
}

async function initial() {
  data = await fetchData();
  setAnnual();
  setPosition();
}