const imageHtml = '<img src="$image-url" alt="">';

const activityHtmlArr = [
  '<div class="sub-section">',
    '<div class="info">',
      '<div class="info-section">',
        '<span class="title">名稱</span>',
        '<span>$title</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">時間</span>',
        '<span>$time</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">地點</span>',
        '<span>$location</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">書目</span>',
        '<span>$book</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">導讀人</span>',
        '<span>$leader</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">內容摘述</span>',
      '</div>',
      '<div class="info-section">',
        '<span>$introduction</span>',
      '</div>',
    '</div>',
    '<div class="huge-circle">',
      // '$images',
      '<img src="$image-url" alt="">',
      '<div class="fake-cover"></div>',
    '</div>',
  '</div>'
];
const activityHtml = activityHtmlArr.join('');

const speakHtmlArr = [
  '<div class="sub-section">',
    '<div class="huge-circle">',
      // '$images',
      '<img src="$image-url" alt="">',
      '<div class="fake-cover"></div>',
    '</div>',
    '<div class="info">',
      '<div class="info-section">',
        '<span class="title">名稱</span>',
        '<span>$title</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">時間</span>',
        '<span>$time</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">地點</span>',
        '<span>$location</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">講座</span>',
        '<span>$leader</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">內容摘述</span>',
      '</div>',
      '<div class="info-section">',
        '<span>$introduction</span>',
      '</div>',
    '</div>',
  '</div>'
];
const speakHtml = speakHtmlArr.join('');

const judgeHtmlArr = [
  '<div class="sub-section">',
    '<div class="info">',
      '<div class="info-section">',
        '<span class="title">名稱</span>',
        '<span>$title</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">時間</span>',
        '<span>$time</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">地點</span>',
        '<span>$location</span>',
      '</div>',
      '<div class="info-section">',
        '<span class="title">內容摘述</span>',
      '</div>',
      '<div class="info-section">',
        '<span>$introduction</span>',
      '</div>',
    '</div>',
    '<div class="image-section">$images</div>',
  '</div>'
];
const judgesHtml = judgeHtmlArr.join('');

let avtivitiesData = [];
let speaksData = []
let judgesData = [];

async function fetchData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/activityRecord/getAll")
  let rawData = await raw.json()
  
  avtivitiesData = [];
  speaksData = []
  judgesData = [];

  if (rawData) {
    for (item of rawData) {
      let { id, author, book, content, image_link1, image_link2, location, time, title, type } = item
      const data = {
        id, author, book, content, image_link1, image_link2, location, time, title, type
      };
      if (type == '共讀活動') {
        avtivitiesData.push(data)
      } else if (type == '專題演講') {
        speaksData.push(data)
      } else if (type == '培訓評審') {
        judgesData.push(data)
      } else {
        console.log('error data: ', data);
      }
    }
  }
}

// async function fetchAtivityData() {
//   let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/activityRecord/getAll")
//   let rawData = await raw.json()
//   // let rawData = [
//   //   {
//   //     images: [
//   //       'assets/img/result/共讀活動1.jpg',
//   //       'assets/img/result/共讀活動2.jpg'
//   //     ],
//   //     title: '【我如何真確理解世界】專書閱讀導讀會',
//   //     time: '110年4月19日(星期一)早上10:00-12:00',
//   //     location: '國立清華大學旺宏館3F遠距教室B',
//   //     book: '我如何真確理解世界',
//   //     leader: '鄭國威 泛科知識股份有限公司 共同創辦人暨知識長/臺灣數位文化協會理事長',
//   //     introduction: '是什麼樣的人生歷程與思辨，讓漢斯．羅斯林成為舉世聞名的全球公衛學家與教育家，並以《真確》揭露世界的真相，扭轉我們的直覺偏誤？本書為羅斯林逝世前堅持寫下的人生自述。他說：「《真確》探討我們為何如此難以理解世局的發展，本書則與我個人和我如何逐漸理解這個世界有關。總而言之，這本書中沒有什麼數據，它探討我和人們相遇、交流的經過。」'
//   //   }
//   // ]

//   let data = []

//   if (rawData) {
//     for (item of rawData) {
//       let { title, time, location, book, leader, introduction, images } = item
//       data.push({
//         title, time, location, book, leader, introduction, images
//       })
//     }
//   }

//   return data
// }

// async function fetchSpeaksData() {
//   let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/meeting/getAll")
//   let rawData = await raw.json()
  
//   // let rawData = [
//   //   {
//   //     images: [
//   //       'assets/img/result/演講1.jpg',
//   //       'assets/img/result/演講2.jpg'
//   //     ],
//   //     title: '一日閱讀心得寫作速成班(中區)',
//   //     time: '110年5月6日(星期四 )上午9:30至12:00',
//   //     location: '國立清華學第二綜合大樓8樓',
//   //     leader: '孫玉平 臺中科技大學組長',
//   //     introduction: '人事室於110年5月6日(星期四 )邀請國家文官學院專書閱讀心得寫作競賽金椽獎(第一名!)作者來清華大學傳授閱讀心得及寫作技巧。\n臺中科技大學孫玉平組長於109年以【是反智，也是人性–《美國的反智傳統》給我們的啟示】短文勇奪競賽第二領域第一名。在其文章內可以看到筆者以簡易圖表告訴讀者該書之中心思想，由認識美國社會反智傳統理解相關國際議題，並思索對同為移民社會臺灣的啟示，縱使對美國歷史不甚理解之讀者，亦可輕易從中了解到美國知識分子或當政者正面臨何種的困境與期許。如何閱讀生硬的書籍，並從中擷取精華告知讀者該書意旨，正是孫組長多年來的閱讀經驗養成。'
//   //   }
//   // ]
//   let data = []

//   if (rawData) {
//     for (item of rawData) {
//       item.time = item.time.replace("|", " ")

//       data.push(item)
//     }
//   }

//   return data
// }

// async function fetchJudgesData() {
//   let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/book/getAll")
//   let rawData = await raw.json()
  
//   // let rawData = [
//   //   {
//   //     images: [
//   //       'assets/img/result/培訓2.jpg',
//   //       'assets/img/result/培訓3.jpg'
//   //     ],
//   //     title: '110年國立清華大學公務人員專書閱讀書展',
//   //     time: '110年5月3日起至110年6月4日',
//   //     location: '國立清華大學南大分館及總圖1樓',
//   //     introduction: '為落實終身學習，倡導閱讀風氣，舉辦本校「110年度公務人員指定專書閱讀主題書展暨導讀會」，其中主題書展於110年5月3日至5月14日(圖書館南大分館二樓)展示、 110年5月24日至6月4日(總圖書館一樓知識集弓形側牆)展示。'
//   //   }
//   // ]
//   let data = []

//   if (rawData) {
//     for (item of rawData) {
//       data.push(item)
//     }
//   }

//   return data
// }
function ScrollTo(idx) {
  console.log(idx)
  if (screen.width >= 768) {
    document.getElementsByClassName('section')[idx].scrollIntoView();
  } else {
    selectTab(idx);
  }
}

function noScroll() {
  window.scrollTo(0,0)
}

function toggleMenu() {
  document.getElementById('expand-menu').classList.toggle('expand');
  document.getElementById('blur-cover').classList.toggle('expand');
  document.getElementById('body').classList.toggle('fixed');
}
function setBanner(e, imgs) {
  console.log('setBanner', e, imgs);
  const times = imgs.length;
  let idx = 1
  setInterval(() => {
    e.src = imgs[idx];
    idx = (idx+1)%times;
    // console.log(e.src)
  }, 3000);
}
function setActivities() {
  const images = []
  const activity = document.getElementById('activity');
  console.log(activity);
  activity.innerHTML = avtivitiesData.map(e => {
    images.push([e.image_link1, e.image_link2]);
    // const images = e.images.map(img => imageHtml.replace('$image-url', img)).join('');
    return activityHtml.replace('$title', e.title).replace('$time', e.time).replace('$location', e.location).replace('$book', e.book).replace('$leader', e.author).replace('$introduction', e.content).replace('$image-url', images[0])
  }).join('');

  const items = document.querySelectorAll('#activity .huge-circle img')
  for (let i = 0; i < items.length; i+=1) {
    setBanner(items[i], images[i])
  }
}
function setSpeaks() {
  const images = []
  const speak = document.getElementById('speak');
  console.log(speak);
  speak.innerHTML = speaksData.map(e => {
    images.push([e.image_link1, e.image_link2]);
    // const images = e.images.map(img => imageHtml.replace('$image-url', img)).join('');
    return speakHtml.replace('$title', e.title).replace('$time', e.time).replace('$location', e.location).replace('$leader', e.author).replace('$introduction', e.content).replace('$image-url', images[0])
  }).join('');

  const items = document.querySelectorAll('#speak .huge-circle img')
  for (let i = 0; i < items.length; i+=1) {
    setBanner(items[i], images[i])
  }
}
function setJudges() {
  const judge = document.getElementById('judge');
  console.log(judge);
  judge.innerHTML = judgesData.map(e => {
    const imgs = [e.image_link1, e.image_link2];
    const images = imgs.map(img => imageHtml.replace('$image-url', img)).join('');
    return judgesHtml.replace('$title', e.title).replace('$time', e.time).replace('$location', e.location).replace('$introduction', e.content).replace('$images', images)
  }).join('');
}
let type = 0;
const data = [[],[],[]], title = ['共讀活動', '專題演講', '培訓評審'], idList = ['activity', 'speak', 'judge'];
function setMobile() {
  data[0] = avtivitiesData.map(e => activityHtml.replace('$title', e.title).replace('$time', e.time).replace('$location', e.location).replace('$book', e.book).replace('$leader', e.author).replace('$introduction', e.content).replace('$image-url', e.image_link1)).join('');
  data[1] = speaksData.map(e => speakHtml.replace('$title', e.title).replace('$time', e.time).replace('$location', e.location).replace('$leader', e.author).replace('$introduction', e.content).replace('$image-url', e.image_link1)).join('');
  data[2] = judgesData.map(e => {
    const imgs = [e.image_link1, e.image_link2];
    const images = imgs.map(img => imageHtml.replace('$image-url', img)).join('');
    return judgesHtml.replace('$title', e.title).replace('$time', e.time).replace('$location', e.location).replace('$introduction', e.content).replace('$images', images)
  }).join('');
}

function selectTab(idx) {
  const mobile = document.getElementById('mobile-section');
  
  document.getElementsByClassName('icon-block')[type].classList.remove('selected');
  mobile.classList.remove(idList[type])
  type = idx;
  document.getElementsByClassName('icon-block')[type].classList.add('selected');

  mobile.classList.add(idList[type])
  mobile.innerHTML = data[type];
  document.getElementById('mobile-title').textContent = title[type];

  if (type !== 2) {
    let images = []
    if (type === 0) {
      images = avtivitiesData.map(e => [e.image_link1, e.image_link2])
    } else if (type === 1) {
      images = speaksData.map(e => [e.image_link1, e.image_link2])
    }
    console.log('images', images);
    const items = document.querySelectorAll('.huge-circle img')
    for (let i = 0; i < items.length; i+=1) {
      setBanner(items[i], images[i])
    }
  }
}

async function initial() {
  await fetchData();
  // avtivitiesData = await fetchAtivityData()
  // speaksData = await fetchSpeaksData()
  // judgesData = await fetchJudgesData()

  if (screen.width >= 768) {
    setActivities();
    setSpeaks();
    setJudges();
  } else {
    setMobile();
    selectTab(0);
  }
  console.log(screen.width);
}