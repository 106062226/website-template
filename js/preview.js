const previewHtmlArr = [
  '<div id="card-$idx" class="preview list">',
    '<div class="img-line">',
      '$img',
    '</div>',
    '<div class="title-line">',
      '<span>$title</span>',
    '</div>',
    '<div class="board-section">',
      '$author',
      '<div class="location-line">',
        '<span>$location</span>',
      '</div>',
      '<div class="dotted-line"></div>',
      '<div class="date-line">',
        '<span>$date</span>',
      '</div>',
      '<div class="dotted-line"></div>',
      '<div class="people-line">',
        '<span class="tips">人限：</span><span>$people</span>',
      '</div>',
      '<div class="dotted-line"></div>',
      '$registration',
    '</div>',
  '</div>'
];
const authorHtmlArr = [
  '<div class="author-line">',
    '<span class="author-name">$author_name$author_occup</span>',
    '$author_title',
  '</div>',
  '<div class="dotted-line"></div>'
];
const registrationHtmlArr = [
  '<div class="registration-line">',
    '<a class="registration-btn" href="$url" target="_blank">我要報名！</a>',
  '</div>'
];
const contactHtmlArr = [
  '<div class="contact-line">',
    '<span class="text">逕洽聯絡人</span>',
    '<div class="contact-person">',
      '<span class="name">$contact-name</span>',
      '<span class="phone">$contact-phone</span>',
    '</div>',
  '</div>'
];
const previewHtml = previewHtmlArr.join('');
const authorHtml = authorHtmlArr.join('');
const authorTitleHtml = '<span class="author-title">$author_title</span>';
const registrationHtml = registrationHtmlArr.join('');
const contactHtml = contactHtmlArr.join('');
const default_img = '<img class="default-img" src="./assets/speech.svg">';

let data = [];

function getEvents() {
  const ans = [];
  for (let i = 0; i < 10; i++) {
    ans.push({
      desc: 'aaaaaaa',
        people: 1,
        image_url: '',
        url: '',
      date: '2020/03/17',
      location: '新竹市東區xx路' + Math.floor(Math.random() * 500) + '號',
      title: '這是會議編號' + Math.floor(Math.random() * 100),
    });
  }
  return ans;
}

function timeVal(d, t) {
  // console.log(d, t)
  const times = [d, t];
  const years = times[0].split('年')
  const months = years[1].split('月')
  const year = Number(years[0]), month = Number(months[0]), date = Number(months[1].split('日')[0]);
  const hour = Number(times[1].split(':')[0]), min = Number(times[1].split(':')[1].split('-')[0]);
  const ans = (((year * 12 + month) * 31 + date) * 24 + hour) * 60 + min;
  // console.log(year, month, date, ans);
  return ans;
}
async function fetchData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/activity/getAll")
  let rawData = await raw.json()
  rawData.forEach(d => {
    d.timeVal = timeVal(d.time.split('|')[0], d.time.split('|')[1])
  });
  rawData.sort(function(a, b) {
    return a.timeVal < b.timeVal ? 1 : -1;
  });
  let data = []


  if (rawData) {
    for (item of rawData) {
      let { title, desc, author_name, author_title, time, location, seat_limit, link, thumbnail, contact_name, contact_phone } = item
      data.push({
        desc,
        title,
        author_name,
        author_title,
        location,
        contact: {
          name: contact_name,
          phone: contact_phone
        },
        people: seat_limit,
        img_url: thumbnail,
        url: link,
        time: time.split("|")[1],
        date: time.split("|")[0]
      })
    }
  }
  return data
}

function ScrollTo(idx) {
  if (screen.width >= 768) {
    document.getElementsByClassName('section')[idx].scrollIntoView();
  }
}

function toggleMenu() {
  document.getElementById('expand-menu').classList.toggle('expand');
  document.getElementById('blur-cover').classList.toggle('expand');
  document.getElementById('body').classList.toggle('fixed');
}

function setPreview() {
  const preview = document.getElementById('preview-section');
  const newHtml = data.map((p, idx) => {
    const authorTitle = p.author_title !== "" ? authorTitleHtml.replace('$author_title', p.author_title) : '';
    const author = p.author_name !== ""
      ? authorHtml
        .replace('$author_name', p.author_name)
        .replace('$author_occup', p.author_occup ? ' ' + p.author_occup : '')
        .replace('$author_title', authorTitle)
      : '';
    
    const registration = p.url !== ""
      ? registrationHtml.replace('$url', p.url)
      : (p.contact && p.contact.name !== ""
        ? contactHtml.replace('$contact-name', p.contact.name).replace('$contact-phone', p.contact.phone)
        : '');
    return previewHtml
      .replace('$idx', idx)
      .replace('$href', p.href)
      .replace('$title', p.title)
      .replace('$img', (p.img_url) ? `<img src="${p.img_url}">`: default_img)
      .replace('$author', author)
      .replace('$location', p.location)
      .replace('$date', p.date + ' ' + p.time)
      .replace('$people', p.people)
      .replace('$url', p.url)
      .replace('$registration', registration);
  }).join('');
  preview.innerHTML = newHtml;
}

function getParameterByName(name, url = window.location.href) {
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function setSelectedItem() {
  const id = getParameterByName('id');
  if (id !== null) {
    const item = document.getElementById('card-' + id)
    item.classList.add('selected');
    item.scrollIntoView({block: "center"});
  }
}

async function initial() {
  data = await fetchData()
  setPreview();
  setSelectedItem();
  console.log(screen.width);
}