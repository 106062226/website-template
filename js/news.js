const eventHtml = '<div class="event list" onclick="Goto(\'$href\')"><div class="date">$date</div><div class="title">$title</div></div>';
const confHtml = '<div class="conf list"><div class="tag-section"><div class="date">$date</div><div class="location">$location</div></div><div class="title">$title</div></div>';
const studiesHtml = '<div class="studies list" onclick="Goto(\'$href\')"><div class="title">$title</div></div>';
const more = '<span class="more">更多...</span>'

let eventsData = [];
let confsData = []
let studiesData = [];


function timeVal(d, t) {
  // console.log(d, t)
  const times = [d, t];
  const years = times[0].split('年')
  const months = years[1].split('月')
  const year = Number(years[0]), month = Number(months[0]), date = Number(months[1].split('日')[0]);
  const hour = Number(times[1].split(':')[0]), min = Number(times[1].split(':')[1].split('-')[0]);
  const ans = (((year * 12 + month) * 31 + date) * 24 + hour) * 60 + min;
  // console.log(year, month, date, ans);
  return ans;
}
async function fetchEventData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/activity/getAll")
  let rawData = await raw.json()
  rawData.forEach(d => {
    d.timeVal = timeVal(d.time.split('|')[0], d.time.split('|')[1])
  });
  rawData.sort(function(a, b) {
    return a.timeVal < b.timeVal ? 1 : -1;
  });
  let data = []

  if (rawData) {
    for (item of rawData) {
      let { title, desc, author_name, author_title, time, location, seat_limit, link, thumbnail, contact_name, contact_phone } = item
      data.push({
        desc,
        title,
        author_name,
        author_title,
        location,
        contact: {
          name: contact_name,
          phone: contact_phone
        },
        people: seat_limit,
        image_url: thumbnail,
        url: link,
        time: time.split("|")[1],
        date: time.split("|")[0]
      })
    }
  }

  return data
}

async function fetchMeetingData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/meeting/getAll")
  let rawData = await raw.json()
  let data = []

  if (rawData) {
    for (item of rawData) {
      const { place, title } = item;
      const time = item.time.replace("|", " ")
      // console.log('item', item);

      data.push({
        time,
        place,
        title
      })
    }
  }

  return data
}

async function fetchBooksData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/book/getAll")
  let rawData = await raw.json()
  let data = []

  if (rawData) {
    for (item of rawData) {
      if (item.type === "好書推薦") data.push(item)
    }
  }

  return data
}

function ScrollTo(idx) {
  if (screen.width >= 768) {
    document.getElementsByClassName('section')[idx].scrollIntoView();
  } else {
    selectTab(idx);
  }
}

function noScroll() {
  window.scrollTo(0,0)
}

function toggleMenu() {
  document.getElementById('expand-menu').classList.toggle('expand');
  document.getElementById('blur-cover').classList.toggle('expand');
  document.getElementById('body').classList.toggle('fixed');
}
// function getConfs() {
//   const ans = [];
//   for (let i = 0; i < 10; i++) {
//     ans.push({
//       date: '2020/03/17',
//       location: '新竹市東區xx路' + Math.floor(Math.random() * 500) + '號',
//       title: '這是會議編號' + Math.floor(Math.random() * 100),
//     });
//   }
//   return ans;
// }
// function getStudies() {
//   const ans = [];
//   for (let i = 0; i < 10; i++) {
//     ans.push({
//       title: '這是書目編號' + Math.floor(Math.random() * 100),
//       url: 'https://colorhunt.co/palette/1223'
//     });
//   }
//   return ans;
// }
function setMoreData(target, datablks) {
  const tg = document.getElementById(target);
  const dataMore = datablks.join('');
  tg.innerHTML = datablks.slice(0, 5).join('') + more;
  const moreButton = document.querySelectorAll(`#${target} .more`)
  if (moreButton.length > 0) {
    moreButton[0].addEventListener('click', function() {
      tg.innerHTML = dataMore;
    }, false);
  }
}
function setEvents() {
  const eventsDataBlocks = eventsData.map((e, idx) => eventHtml.replace('$date', e.date).replace('$title', e.desc).replace('$href', `preview.html?id=${idx}`));
  if (eventsDataBlocks.length > 5) setMoreData('event', eventsDataBlocks)
  else document.getElementById('event').innerHTML = eventsDataBlocks.join('');
}
function setConfs() {
  const confsDataBlocks = confsData.map(e => confHtml.replace('$date', e.time).replace('$title', e.title).replace('$location', e.place));
  if (confsDataBlocks.length > 5) setMoreData('conf', confsDataBlocks)
  else document.getElementById('conf').innerHTML = confsDataBlocks.join('');
}
function setStudies() {
  const studiesDataBlocks = studiesData.map((e, idx) => studiesHtml.replace('$title', e.name).replace('$href', `annual-books.html?id=${idx}`));
  if (studiesDataBlocks.length > 5) setMoreData('study', studiesDataBlocks)
  else document.getElementById('study').innerHTML = studiesDataBlocks.join('');
}
let type = 0;
const data = [[],[],[]], dataMore = [[], [], []], title = ['活動名稱', '會議資訊', '好書推薦'];
function setMobile() {
  const eventsDataBlocks = eventsData.map((e, idx) => eventHtml.replace('$date', e.date).replace('$title', e.desc).replace('$href', `preview.html?id=${idx}`));
  if (eventsDataBlocks.length > 5) {
    data[0] = eventsDataBlocks.slice(0, 5).join('') + more;
    dataMore[0] = eventsDataBlocks.join('');
  } else {
    data[0] = eventsDataBlocks.join('');
  }
  const confsDataBlocks = confsData.map(e => confHtml.replace('$date', e.time).replace('$title', e.title).replace('$location', e.place));
  if (confsDataBlocks.length > 5) {
    data[1] = confsDataBlocks.slice(0, 5).join('') + more;
    dataMore[1] = eventsDataBlocks.join('');
  } else {
    data[1] = confsDataBlocks.join('');
  }
  const studiesDataBlocks = studiesData.map((e, idx) => studiesHtml.replace('$title', e.name).replace('$href', `annual-books.html?id=${idx}`));
  if (studiesDataBlocks.length > 5) {
    data[2] = studiesDataBlocks.slice(0, 5).join('') + more;
    dataMore[2] = eventsDataBlocks.join('');
  } else {
    data[2] = studiesDataBlocks.join('');
  }
  // data[0] = eventsData.map((e, idx) => eventHtml.replace('$date', e.date).replace('$title', e.desc).replace('$href', `preview.html?id=${idx}`)).join('');
  // data[1] = confsData.map(e => confHtml.replace('$date', e.time).replace('$title', e.title).replace('$location', e.place)).join('');
  // data[2] = studiesData.map((e, idx) => studiesHtml.replace('$title', e.name).replace('$href', `annual-books.html?id=${idx}`)).join('');
}

function selectTab(idx) {
  document.getElementsByClassName('icon-block')[type].classList.remove('selected');
  type = idx;
  document.getElementsByClassName('icon-block')[type].classList.add('selected');

  const mobile = document.getElementById('mobile-section');
  mobile.innerHTML = data[type];
  document.getElementById('mobile-title').textContent = title[type];

  const moreButton = mobile.getElementsByClassName('more');
  if (moreButton.length > 0) {
    moreButton[0].addEventListener('click', function() {
      mobile.innerHTML = dataMore[type];
    }, false)
  }
}

async function initial() {
  eventsData = await fetchEventData()
  confsData = await fetchMeetingData()
  studiesData = await fetchBooksData()

  if (screen.width >= 768) {
    setEvents();
    setConfs();
    setStudies();
  } else {
    setMobile();
    selectTab(0);
  }
  console.log(screen.width);
}