// Start for Member

const teamHtmlArr = [
  '<div class="team-content">',
    '<div class="content-title">',
      '<span>第$team-count-ch組 - $team-title</span>',
    '</div>',
    '<div class="content-box">',
      '<div class="content-line">',
        '<span class="title">辦理時程</span>',
        '<span class="text">$team-time</span>',
      '</div>',
      '<div class="content-line">',
        '<span class="title">彙整方式</span>',
        '<span class="text">$team-gather</span>',
      '</div>',
      '<div class="content-line">',
        '<span class="title">主辦單位</span>',
        '<span class="text">$team-host</span>',
      '</div>',
    '</div>',
    '<div class="contact-window">',
    '$team-contact',
    '</div>',
  '</div>'
];
const contactHtmlArr = [
  '<div class="contact-line">',
    '$disc',
    '<div class="people-section">',
      '$people',
    '</div>',
  '</div>'
];
const personHtmlArr = [
  '<div class="tags">',
    '<span class="school">$school</span>',
    '<span class="person">$name$occup</span>',
    '<div class="phone">',
      '<img src="assets/phone-call.svg">',
      '<a href="tel:$phone-no-dash">$phone</a>',
    '</div>',
    '<div class="email">',
      '<img src="assets/mail.svg">',
      '<a href="mailto:$mail">$mail</a>',
    '</div>',
  '</div>',
]
const targetCarouselHtmlArr = [
  '<div class="carousel-item">',
    '<img class="d-block" src="$link" alt="">',
    '<span class="tips">$intro</span>',
  '</div>'
]
const honorHtmlArr = [
  '<div class="contact-line">',
    '$tag',
    '<div class="people-section">',
      '$people',
    '</div>',
  '</div>'
];
const honorPersonHtmlArr = [
  '<div class="tags">',
    '<span class="race">$competition</span>',
    '<span class="award">$award</span>',
    '<span class="person">$title</span>',
  '</div>',
];
const teamHtml = teamHtmlArr.join('');
const contactHtml = contactHtmlArr.join('');
const personHtml = personHtmlArr.join('');
const targetCarouselHtml = targetCarouselHtmlArr.join('');
const honorHtml = honorHtmlArr.join('');
const honorPersonHtml = honorPersonHtmlArr.join('');

async function fetchTargetData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/target/getAll")
  let rawData = await raw.json()
  let data = []

  if (rawData) {
    for (item of rawData) {
      data.push(item);
    }
  }

  return data
}
async function fetchTargetTextData() {
  let raw = await fetch("http://staff140.et.nthu.edu.tw:9000/api/targetText/getAll")
  let rawData = await raw.json()
  let data = []

  if (rawData) {
    for (item of rawData) {
      data.push(item);
    }
  }

  return data[0]
}

async function fetchMemberData() {
  let rawT = await fetch("http://staff140.et.nthu.edu.tw:9000/api/team/getAll")
  let rawM = await fetch("http://staff140.et.nthu.edu.tw:9000/api/member/getAll")
  let rawDataT = await rawT.json()
  let rawDataM = await rawM.json()
  let data = []

  if (rawDataT) {
    for (item of rawDataT) {
      item.member = [];
      data.push(item);
    }
  }

  if (rawDataM) {
    for (item of rawDataM) {
      const teamIdx = rawDataT.findIndex(team => team.id === item.team);
      data[teamIdx].member.push(item);
    }
  }

  return data
}

async function fetchAwardData() {
  let rawT = await fetch("http://staff140.et.nthu.edu.tw:9000/api/awardtag/getAll")
  let rawM = await fetch("http://staff140.et.nthu.edu.tw:9000/api/award/getAll")
  let rawDataT = await rawT.json()
  let rawDataM = await rawM.json()
  let data = {team: [], person: []}

  if (rawDataM) {
    for (item of rawDataM) {
      data[item.identity].push(item);
    }

    const res = {}
    let tagList = []
    for (item of data.team) {
      const tagOriIdx = rawDataT.findIndex(tag => tag.id === item.tag);
      if (item.tag < 0) {
        tagList.push({id: -1, title: '',  member: [item] });
      } else {
        const tagIdx = tagList.findIndex(tag => tag.id === item.tag)
        if (tagIdx >= 0) {
          tagList[tagIdx].member.push(item);
        } else {
          tagList.push(rawDataT[tagOriIdx]);
          tagList[tagList.length-1].member = [item];
        }
      }
    }
    data.team = tagList;
    
    tagList = []
    for (item of data.person) {
      const tagOriIdx = rawDataT.findIndex(tag => tag.id === item.tag);
      if (item.tag < 0) {
        tagList.push({id: -1, title: '',  member: [item] });
      } else {
        const tagIdx = tagList.findIndex(tag => tag.id === item.tag)
        if (tagIdx >= 0) {
          tagList[tagIdx].member.push(item);
        } else {
          tagList.push(rawDataT[tagOriIdx]);
          tagList[tagList.length-1].member = [item];
        }
      }
    }
    data.person = tagList;
  }

  return data
}

// const data = [{
//   teamTitle: '競賽作品蒐集',
//   teamTime: '7月1日協辦單位截止收件。7月10日送清華大學彙整。',
//   teamGather: '如附表1、附表2 (請逕洽協辦單位)',
//   teamHost: '清華大學-吳羚菀專員 &lt;<a href="tel:035731319">03-5731319</a>; <a href="mailto:lywu@mx.nthu.edu.tw">lywu@mx.nthu.edu.tw</a>&gt;',
//   contact: [{
//     disc: '北一區',
//     people: [{
//       school: '國立臺北教育大學',
//       name: '張雯雅',
//       occup: '秘書',
//       phone: '02-2732-1104#82289',
//       mail: 'wenyachang@tea.ntue.edu.tw'
//     }]
//   }, {
//     disc: '北二區',
//     people: [{
//       school: '國立陽明交通大學附設醫院',
//       name: '林嘉振',
//       occup: '組員',
//       phone: '03-9325192#71662',
//       mail: '13516@ymuh.ym.edu.tw'
//     }]
//   }, {
//     disc: '北三區',
//     people: [{
//       school: '國立臺灣師範大學',
//       name: '莊珮怡',
//       occup: '行政專員',
//       phone: '02-7749-1301',
//       mail: '1070921@ntnu.edu.tw'
//     }]
//   }, {
//     disc: '北四區',
//     people: [{
//       school: '國立政治大學',
//       name: '楊宇晴',
//       occup: '行政專員',
//       phone: '02-29393091#63318',
//       mail: '3475@nccu.edu.tw'
//     }]
//   }, {
//     disc: '北五區',
//     people: [{
//       school: '國立體育大學',
//       name: '劉南琦',
//       occup: '組員',
//       phone: '03-3283201#1632',
//       mail: 'fayer1108@ntsu.edu.tw'
//     }]
//   }, {
//     disc: '花東區',
//     people: [{
//       school: '國立臺東專科學校',
//       name: '左家丞',
//       occup: '組員',
//       phone: '089-226389 #2502',
//       mail: 'zuo07030808@ntc.edu.tw'
//     }]
//   }, {
//     disc: '中一區',
//     people: [{
//       school: '國立暨南國際大學',
//       name: '賴苡修',
//       occup: '組員',
//       phone: '049-2910960#2517',
//       mail: 'yihsiulai@ncnu.edu.tw'
//     }]
//   }, {
//     disc: '中三區',
//     people: [{
//       school: '國立嘉義大學',
//       name: '陳莉函',
//       occup: '組員',
//       phone: '05-2717192',
//       mail: 'renai@mail.ncyu.edu.tw'
//     }]
//   }, {
//     disc: '南一區',
//     people: [{
//       school: '國立高雄師範大學',
//       name: '李思穎',
//       occup: '組員',
//       phone: '07-7172930#1477',
//       mail: 'lillian@nknucc.nknu.edu.tw'
//     }]
//   }, {
//     disc: '南二區',
//     people: [{
//       school: '國立高雄餐旅大學',
//       name: '蔡明瑾',
//       occup: '行政助理',
//       phone: '07-8060505#11104',
//       mail: 'emilytsai@mail.nkuht.edu.tw'
//     }]
//   }, {
//     disc: '南三區',
//     people: [{
//       school: '國立成功大學醫學院附設醫院',
//       name: '翁敏皓',
//       occup: '專員',
//       phone: '06-2353535#2049',
//       mail: 'n048040@mail.hosp.ncku.edu.tw'
//     }]
//   }, {
//     disc: '全國合作 高中職以下',
//     people: [{
//       school: '教育部國民及學前教育署',
//       name: '潘俐樺',
//       occup: '專案助理',
//       phone: '04-37061505',
//       mail: 'e-p210@mail.k12ea.gov.tw'
//     }, {
//       school: '國家圖書館',
//       name: '徐曉盈',
//       occup: '組員',
//       phone: '02-23619132#159',
//       mail: 'person@ncl.edu.tw'
//     }]
//   }]
// }, {
//   teamTitle: '計畫及E時數蒐集',
//   teamTime: '7月15日主、協辦學校截止收件。7月26日協辦學校送主辦學校彙整。8月9日主辦學校送國立清華大學彙整。',
//   teamGather: '如附表4、附表12 (請逕洽協辦單位)',
//   teamHost: '清華大學-林宛臻組員 &lt;<a href="tel:035731320">03-5731320</a>; <a href="mailto:linwc@mx.nthu.edu.tw">linwc@mx.nthu.edu.tw</a> &gt;',
//   contact: [{
//     people: [{
//       school: '國立清華大學',
//       name: '林宛臻',
//       occup: '組員',
//       phone: '03-5731320',
//       mail: 'linwc@mx.nthu.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立聯合大學',
//       name: '楊依倢',
//       occup: '組員',
//       phone: '037-381051',
//       mail: 'yang1j0311@nuu.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '臺灣圖書館',
//       name: '佘玉蘭',
//       occup: '約僱服務員',
//       phone: '02-29266888#8751',
//       mail: 'yulan@mail.ntl.edu.tw'
//     }]
//   }]
// }, {
//   teamTitle: '「導讀會」及「讀書會」場次蒐集',
//   teamTime: '7月15日主、協辦學校截止收件。7月26日協辦學校送主辦學校彙整。8月9日主辦學校送國立清華大學彙整。',
//   teamGather: '如附表3、附表5(請逕洽協辦單位)',
//   teamHost: '國立臺灣海洋大學(江芳妤組員) &lt;<a href="tel:0224622192">02-2462-2192#1153</a>; <a href="mailto:slima@mail.ntou.edu.tw">slima@mail.ntou.edu.tw</a>&gt;',
//   contact: [{
//     people: [{
//       school: '國立臺灣海洋大學',
//       name: '江芳妤',
//       occup: '組員',
//       phone: '02-2462-2192#1153',
//       mail: 'slima@mail.ntou.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立臺灣藝術大學',
//       name: '李旻純',
//       occup: '組員',
//       phone: '02-2272-2181#1505',
//       mail: 'mayqoo@ntua.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立東華大學',
//       name: '葉珏廷',
//       occup: '組員',
//       phone: '03-8906059',
//       mail: 'missyehnu@gms.ndhu.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立海洋生物博物館',
//       name: '邱芝琪',
//       occup: '組員',
//       phone: '08-8825001#5541',
//       mail: 'taieachi@nmmba.gov.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立屏東大學',
//       name: '黃秦香',
//       occup: '組員',
//       phone: '08-7663800#10309',
//       mail: 'shiang@mail.nptu.edu.tw'
//     }]
//   }]
// }, {
//   teamTitle: '與作者有約專題演講、採購每月一書圖書及主題書展蒐集',
//   teamTime: '7月15日主、協辦學校截止收件。7月26日協辦學校送主辦學校彙整。8月9日主辦學校送國立清華大學彙整。',
//   teamGather: '如附表3、6、7 (請逕洽協辦單位)',
//   teamHost: '國立臺南藝術大學-廖雪霞主任 &lt;<a href="tel:066930100">06-6930100#1050</a>; <a href="mailto:liawss@tnnua.edu.tw">liawss@tnnua.edu.tw</a>&gt;',
//   contact: [{
//     people: [{
//       school: '國立空中大學',
//       name: '賴君恬',
//       occup: '組員',
//       phone: '02-22829355#5913',
//       mail: 'aikimik@mail.nou.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '宜蘭大學',
//       name: '邱玲裕',
//       occup: '組員',
//       phone: '03-935-7400#7024',
//       mail: 'lichiu@niu.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立陽明交通大學',
//       name: '張旖宸',
//       occup: '組員',
//       phone: '03-571-2121＃52241',
//       mail: 'yichen0621@nctu.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立臺南藝術大學',
//       name: '廖雪霞',
//       occup: '主任',
//       phone: '06-6930100#1050',
//       mail: 'liawss@tnnua.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立金門大學',
//       name: '陳柏嘉',
//       occup: '組員',
//       phone: '082-313591',
//       mail: 'vic325238@nqu.edu.tw'
//     }]
//   }]
// }, {
//   teamTitle: '領讀人培訓、各項團體學習技法之培訓、寫作研習及寫作優良頒獎活動蒐集',
//   teamTime: '7月15日主、協辦學校截止收件。7月26日協辦學校送主辦學校彙整。8月9日主辦學校送國立清華大學彙整。',
//   teamGather: '如附表3、8、9、10 (請逕洽協辦單位)',
//   teamHost: '臺南護理專科學校-林桂琴主任 &lt;<a href="tel:062110572">06-2110572</a>; <a href="mailto:vivian@ntin.edu.tw">vivian@ntin.edu.tw</a>&gt;',
//   contact: [{
//     people: [{
//       school: '國立臺灣大學醫學院附設醫院',
//       name: '褚懿慧',
//       occup: '副管理師',
//       phone: '0963-007321',
//       mail: 'flyingcat1724@ntuh.gov.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立臺北大學',
//       name: '吳佩娟',
//       occup: '組員',
//       phone: '02-8674-1111#66058',
//       mail: 'praylord@gm.ntpu.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立科學工藝博物館',
//       name: '陳怡婷',
//       occup: '組員',
//       phone: '07-3800089#8415',
//       mail: 'cyt@mail.nstm.gov.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立屏東科技大學',
//       name: '黃創業',
//       occup: '組員',
//       phone: '08-7703202#6360',
//       mail: 'hcy@mail.npust.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '臺南護理專科學校',
//       name: '林桂琴',
//       occup: '主任',
//       phone: '06-2110572',
//       mail: 'vivian@ntin.edu.tw'
//     }]
//   }]
// }, {
//   teamTitle: '推動強化公務人員終身學習及專書閱讀相關網站（專區）或部落格之蒐集',
//   teamTime: '7月15日主、協辦學校截止收件。7月26日協辦學校送主辦學校彙整。8月9日主辦學校送國立清華大學彙整。',
//   teamGather: '如附表11 (請逕洽協辦單位)',
//   teamHost: '國立虎尾科技大學-蔡佳境組員 &lt;<a href="tel:056315261">05-6315261</a>; <a href="mailto:a90021@nfu.edu.tw">a90021@nfu.edu.tw</a>&gt;',
//   contact: [{
//     people: [{
//       school: '國立臺灣戲曲學院',
//       name: '胡麗晴',
//       occup: '組員',
//       phone: '02-27962666#1121',
//       mail: 'anger1416@tcpa.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國家教育研究院',
//       name: '陳貞儒',
//       occup: '組員',
//       phone: '02-7740-7031',
//       mail: 'kartess@mail.naer.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立虎尾科技大學',
//       name: '蔡佳境',
//       occup: '組員',
//       phone: '05-6315261',
//       mail: 'a90021@nfu.edu.tw'
//     }]
//   }, {
//     people: [{
//       school: '國立臺灣藝術教育館',
//       name: '王文江',
//       occup: '人事管理員',
//       phone: '02-23110574#163',
//       mail: 'jweqr8@linux.arte.gov.tw'
//     }]
//   }]
// }]
function NoToChinese(num) {
  // in 100
  const numArr = ['','一','二','三','四','五','六','七','八','九'];
  if (num > 10) {
    return numArr[num/10-1] + '十' + numArr[num%10];
  } else {
    return numArr[num%10];
  }
}
function setAbout() {
  const body = document.getElementById('member');
  // const newHtml = data.map((d, idx) => {
  //   const contact = d.contact.map(c => {
  //     const people = c.people.map(p => {
  //       return personHtml
  //         .replace('$school', p.school)
  //         .replace('$name', p.name)
  //         .replace('$occup', p.occup)
  //         .replace('$phone-no-dash', p.phone.split('#')[0].replaceAll('-', ''))
  //         .replace('$phone', p.phone)
  //         .replaceAll('$mail', p.mail)
  //     }).join('');
  //     const disc = c.disc ? `<div class="tags left-section"><span class="disc">${c.disc}</span></div>` : '';
  //     return contactHtml
  //       .replace('$disc', disc)
  //       .replace('$people', people)
  //   }).join('');
  //   return teamHtml
  //     .replace('$team-count-ch', NoToChinese(idx+1))
  //     .replace('$team-title', d.teamTitle)
  //     .replace('$team-time', d.teamTime)
  //     .replace('$team-gather', d.teamGather)
  //     .replace('$team-host', d.teamHost)
  //     .replace('$team-contact', contact);
  // }).join('');
  // console.log('memberData', memberData);
  const newHtml = memberData.map((d, idx) => {
    const contact = d.member.map(c => {
      // console.log('c', personHtml, c);
      const people = personHtml
        .replace('$school', c.host)
        .replace('$name', c.name)
        .replace('$occup', c.role)
        .replace('$phone-no-dash', c.phone)
        .replace('$phone', c.phone + (c.extension ? ('#'+c.extension) : ''))
        .replaceAll('$mail', c.email);
      const disc = c.district ? `<div class="tags left-section"><span class="disc">${c.district}</span></div>` : '';
      return contactHtml
        .replace('$disc', disc)
        .replace('$people', people)
    }).join('');
    return teamHtml
      .replace('$team-count-ch', NoToChinese(idx+1))
      .replace('$team-title', d.title)
      .replace('$team-time', d.schedule)
      .replace('$team-gather', d.gather)
      .replace('$team-host', `${d.host} &lt;<a href="tel:${d.phone}">${d.phone}${d.extension?('#'+d.extension):''}</a>; <a href="mailto:${d.email}">${d.email}</a>&gt;`)
      .replace('$team-contact', contact);
  }).join('');
  body.innerHTML = newHtml;
}
function setAward() {
  const honorTeam = document.getElementById('honor-team');
  // console.log('awardData', awardData.team);
  const newHonorHtml = awardData.team.map((d, idx) => {
    let member;
    if (d.member && d.member.length > 0) {
      member = d.member.map(c => {
        // console.log('c', personHtml, c);
        return honorPersonHtml
          .replace('$title', c.title)
          .replace('$competition', c.competition)
          .replace('$award', c.award)
      }).join('');
    } else {
      member = '';
    }
    return honorHtml
      .replace('$tag', d.title ? `<div class="tags left-section"><span class="disc">${d.title}</span></div>` : '')
      .replace('$people', member)
  }).join('');
  honorTeam.innerHTML = newHonorHtml;

  const honorPerson = document.getElementById('honor-person');
  // console.log('awardData', awardData.person);
  const newHonorPersonHtml = awardData.person.map((d, idx) => {
    let member;
    if (d.member && d.member.length > 0) {
      member = d.member.map(c => {
        // console.log('c', personHtml, c);
        return honorPersonHtml
          .replace('$title', c.title)
          .replace('$competition', c.competition)
          .replace('$award', c.award)
      }).join('');
    } else {
      member = '';
    }
    return honorHtml
      .replace('$tag', d.title ? `<div class="tags left-section"><span class="disc">${d.title}</span></div>` : '')
      .replace('$people', member)
  }).join('');
  honorPerson.innerHTML = newHonorPersonHtml;
}
function setTarget() {
  console.log('targetTextData', targetTextData);
  const purpose = document.getElementById('purpose');
  purpose.innerHTML = targetTextData.target + purpose.innerHTML;
  const targetBody = document.getElementById('target-carousel');
  const content = targetData.map(image => targetCarouselHtml.replace('$link', image.link).replace('$intro', image.intro));
  targetBody.innerHTML = content;
  if (targetData.length > 0)
    targetBody.getElementsByClassName('carousel-item')[0].classList.add('active');
}

// End for Member

let type = 0;
let targetData = [];
let targetTextData = [];
let memberData = [];
let awardData = [];

function toggleMenu() {
  document.getElementById('expand-menu').classList.toggle('expand');
  document.getElementById('blur-cover').classList.toggle('expand');
  document.getElementById('body').classList.toggle('fixed');
}

function selectTab(idx) {
  document.getElementsByClassName('icon-block')[type].classList.remove('selected');
  type = idx;
  document.getElementsByClassName('icon-block')[type].classList.add('selected');

  const sections = document.getElementsByClassName('section');
  for (let i = 0; i < sections.length; i += 1) {
    document.getElementsByClassName('section')[i].classList.add('desktop');
    document.getElementsByClassName('section')[i].classList.remove('mobile');
  }
  document.getElementsByClassName('section')[idx].classList.add('mobile');
  document.getElementsByClassName('section')[idx].classList.remove('desktop');
}

function ScrollTo(idx) {
  if (screen.width >= 768) {
    document.getElementsByClassName('section')[idx].scrollIntoView();
  } else {
    selectTab(idx);
  }
}

async function initial() {
  targetData = await fetchTargetData();
  targetTextData = await fetchTargetTextData();
  memberData = await fetchMemberData();
  awardData = await fetchAwardData();
  if (screen.width < 768) selectTab(0);
  setAbout();
  setTarget();
  setAward();
}