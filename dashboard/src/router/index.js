import { createRouter, createWebHashHistory } from 'vue-router'
import Login from '@/views/Login'
import Home from '@/views/Home'
import Activity from '@/views/Activity'
import Meeting from '@/views/Meeting'
import Books from '@/views/Books'
import Files from '@/views/Files'
import OnlineSearchLink from '@/views/OnlineSearchLink'
import ActivityRecord from '@/views/ActivityRecord'
import Target from '@/views/Target'
import Member from '@/views/Member'
import Award from '@/views/Award'
import DefaultInfo from '@/views/DefaultInfo'
import User from '@/views/User'
import sister from '@/views/sister'
import Country from '@/views/Country.csv'

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/activity",
    name: "Activity",
    component: Activity
  },{
    path: "/activityRecord",
    name: "ActivityRecord",
    component: ActivityRecord
  },
  {
    path: "/meeting",
    name: "Meeting",
    component: Meeting
  },
  {
    path: "/books",
    name: "Books",
    component: Books
  },
  {
    path: "/files",
    name: "Files",
    component: Files
  },
  {
    path: "/onlineSearchLinks",
    name: "OnlineSearchLinks",
    component: OnlineSearchLink
  },
  {
    path: "/target",
    name: "Target",
    component: Target
  },
  {
    path: "/member",
    name: "Member",
    component: Member
  },
  {
    path: "/award",
    name: "Award",
    component: Award
  },
  {
    path: "/defaultInfo",
    name: "DefaultInfo",
    component: DefaultInfo
  },
  {
    path: "/user",
    name: "User",
    component: User
  },
  {
    path: "/sister",
    name: "sister",
    component: sister
  },
  {
    path: "/Country",
    name: "Country",
    component: Country
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
