import { ref } from 'vue'
import { useRouter } from 'vue-router'
import { ServerUrl } from '@/api/index'
import { post } from 'axios'
import { ElMessageBox } from 'element-plus'

export default function useLoginForm() {
  // Data
  const account = ref("")
  const password = ref("")
  
  const isLoading = ref(false)

  // Router
  let router = useRouter()

  // Method
  const login = async function() {
    isLoading.value = true;

    // Login
    try {
      let data = await post(`${ServerUrl}/management/user/signin`, {
        account: account.value,
        password: password.value
      })
      
      // Set token and redirect to /
      window.localStorage.setItem("token", data.data.token)
      window.localStorage.setItem("account", account.value)

      router.push("/")
    } catch (error) {
      if (error.response.status == 400) {
        ElMessageBox({
          title: "登入失敗", 
          message: "帳號或密碼錯誤，請重新輸入"
        })
      } else {
        // Server Internal Error or Something else.
        // Anyway, needs to contact with Developer
        ElMessageBox({
          title: "錯誤", 
          message: "請聯絡維護人員"
        })
      }
    }

    isLoading.value = false
  }

  return {
    account,
    password,
    isLoading,

    login
  }
}