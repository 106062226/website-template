import { ServerUrl } from '@/api/index'
import { ref, onMounted } from 'vue'
import { ElMessage } from 'element-plus';
import { post } from 'axios'

export default function UploadFile() {

  // Data
  let fileUploadRoute = ref(`${ServerUrl}/management/file/upload`)
  let fileUploadRequestHeader = ref({})
  let fileTmpLink = ref("")
  let fileTmpId = ref(-1)

  // Method
  // API
  let fileDelete = async (id, callback) => {
    await post(`${ServerUrl}/management/file/delete`, { id },{
      headers: fileUploadRequestHeader.value
    })

    if (callback) callback()
  }

  // UploadFile's Helper Method
  let fileClean = () => {
    fileTmpId.value = -1
    fileTmpLink.value = ""
  }

  let fileCleanAndDelete = async () => {
    if (fileTmpLink.value !== "") {
      await fileDelete(fileTmpId.value, null)
    }

    fileClean()
  }

  let fileCheck = (file) => {
    let isOk = file.type === "image/jpg" || file.type === "image/png" || file.type === "image/jpeg"
    if (!isOk) {
      ElMessage({
        message: "檔案格式不支援，請上傳 jpg 或 png 檔",
        type: "error"
      })
    }
    return isOk
  }

  let fileOnUploadSuccess = ({link, id}) => {
    // 把上一個刪掉
    fileTmpLink.value = `${ServerUrl}/${link}`
    fileTmpId.value = id
  }

  let fileOnUploadRemove = () => {
    fileCleanAndDelete()
  }

  // Hook
  onMounted(() => {
    // 從 Local Storage 中取得 Token
    fileUploadRequestHeader.value = {
      Authorization: window.localStorage.getItem("token")
    }
  })

  return {
    // Data
    fileUploadRoute,
    fileUploadRequestHeader,
    fileTmpLink,
    fileTmpId,

    // Method
    fileDelete,
    fileCheck,
    fileOnUploadSuccess,
    fileOnUploadRemove,
    fileClean,
    fileCleanAndDelete
  }
}