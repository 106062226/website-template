import { ServerUrl } from '@/api/index'
import { onMounted } from 'vue'
import { useRouter, useRoute } from 'vue-router'
import { get } from 'axios'
import { ElMessageBox } from 'element-plus'

export default function CheckToken() {

  let router = useRouter()
  let route = useRoute()

  onMounted(async () => {
    // Check whether the token is present.
    // If jwt token is retrieve, redirect to dashboard page
    // and make subsequent requests with that token.
    // Sure the token may not be a valid one, it can't get 
    // anything with successfule responese. So, it doesn't matter
    // if the attack went to the dashboard.
    let token = window.localStorage.getItem("token")
    
    if (!token && route.path.toLowerCase() !== "/login") {
      ElMessageBox({
        title: "驗證失敗", 
        message: "使用者尚未登入，請至登入頁面重新登入"
      })
      router.push("/login")
      return
    }

    try {
      await get(`${ServerUrl}/management/user/check`, {
        headers: {
          'Authorization': token
        }
      })
    } catch (err) {
      if (route.path.toLowerCase() != "/login")
        ElMessageBox({
          title: "驗證失敗", 
          message: "使用者尚未登入或登入過期，請至登入頁面重新登入"
        })
        window.localStorage.removeItem("token")
        window.localStorage.removeItem("account")
        router.push("/login")
    }
  })

  return {}
}