package Middleware

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/Auth"
)

// JWT Token Verifycation Authentication
func Token() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// Check if token exist
		tokenString := ctx.GetHeader("Authorization")

		if len(tokenString) == 0 || tokenString == "null" {
			ctx.String(http.StatusForbidden, "Not Allowed")
			ctx.Abort()
			return
		}

		// Parse and Verify
		claims, err := Auth.JWTParseAndVerify(tokenString)

		if err != nil {
			fmt.Println(err)
			if err.Error() == "signature is invalid" {
				ctx.String(http.StatusForbidden, "Not Allowed")
				ctx.Abort()
				return
			} else {
				ctx.String(http.StatusInternalServerError, "Internal Server Error")
				ctx.Abort()
				return
			}
		}

		// Set Variable
		ctx.Set("tokenName", claims["name"])

		ctx.Next()
	}
}
