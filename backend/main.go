// 人事處的 Server
package main

import (
	"encoding/base64"
	"fmt"
	"log"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/cs-task-force/server/Api"
	"gitlab.com/cs-task-force/server/DB"
	"gitlab.com/cs-task-force/server/router"
)

func main() {
	fmt.Println("Backend Server for 人事處專案")

	godotenv.Load("key.env")

	// User HMAC Key
	key, err := base64.StdEncoding.DecodeString(os.Getenv("PASSWORD_HMAC_KEY"))

	if err != nil {
		log.Fatalln("base64 decode PASSWORD_HMAC_KEY key failed.")
	}

	Api.PasswordHashKey = key

	// File Loading Path
	Api.FileUploadPath = os.Getenv("STOREAGE_PATH")

	if _, err := os.Stat(Api.FileUploadPath); os.IsNotExist(err) {
		err := os.MkdirAll(Api.FileUploadPath, 0777)

		if err != nil {
			log.Fatalln("create file upload path dir failed.")
		}
	}

	// DB
	DB.Init()

	r := gin.Default()

	// Cors
	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{"http://staff140.et.nthu.edu.tw"},
		AllowHeaders: []string{"Authorization", "Content-Type"},
	}))

	// Setup Router
	router.Setup(r)

	r.Run(":9000")
}

func init() {
	// Load Env File

}
