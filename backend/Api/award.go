package Api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

type awardPayload struct {
	ID              int    `json:"id" binding:"required"`
	Title           string `json:"title" binding:"required"`
	Identity        string `json:"identity" binding:"required"`
	Competition     string `json:"competition" binding:"required"`
	Award           string `json:"award" binding:"required"`
	Tag             int    `json:"tag"`
}

func CreateAward(ctx *gin.Context) {
	var payload awardPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.Award{
		Title:         payload.Title,
		Identity:      payload.Identity,
		Competition:   payload.Competition,
		Award:         payload.Award,
		Tag:           payload.Tag,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetAwards(ctx *gin.Context) {
	var awards []DB.Award

	status := DB.DB.Find(&awards)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []awardPayload

	for _, rec := range awards {
		response = append(response, awardPayload{
			ID:              int(rec.ID),
			Title:           rec.Title,
			Identity:        rec.Identity,
			Competition:     rec.Competition,
			Award:           rec.Award,
			Tag:             int(rec.Tag),
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateAward(ctx *gin.Context) {
	var payload awardPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"title":         payload.Title,
		"identity":      payload.Identity,
		"competition":   payload.Competition,
		"award":         payload.Award,
		"tag":           payload.Tag,
	}

	result := DB.DB.Model(DB.Award{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteAward(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var award DB.Award
	// var file DB.File

	DB.DB.First(&award, "id = ?", payload.ID)

	// if award.Link == "" {
	// 	ctx.String(http.StatusOK, "Ok")
	// 	return
	// }

	// if award.LinkFileID != -1 {
	// 	DB.DB.First(&file, "id = ?", award.LinkFileID)

	// 	err := os.Remove(FileUploadPath + "/" + file.InternalFileName)
	// 	result2 := DB.DB.Model(DB.File{}).Where("id = ?", file.ID).Delete(DB.File{})

	// 	if result2.Error != nil || err != nil {
	// 		ctx.String(http.StatusInternalServerError, "Internal Server Error")
	// 		return
	// 	}
	// }

	result := DB.DB.Model(DB.Award{}).Where("id = ?", award.ID).Delete(DB.Award{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
