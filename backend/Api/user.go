package Api

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/Auth"
	"gitlab.com/cs-task-force/server/DB"
)

/*
 * All api here can assume that the input is clean(filtered)
 * Some middlewares should be implemented or a WAF should be
 * setup.
 */

type userPayload struct {
	ID              int    `json:"id" binding:"required"`
	Account         string `json:"account" binding:"required"`
	Password        string `json:"password" binding:"required"`
	Name            string `json:"name"`
}

type userSigninPayload struct {
	Account  string `json:"account" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func CreateUser(ctx *gin.Context) {
	var payload userPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Hash Password
	hash := hmac.New(sha256.New, PasswordHashKey)
	hash.Write([]byte(payload.Password))
	sum := hash.Sum(nil)

	// Prepare Payload
	data := DB.User{
		Account:       payload.Account,
		Password:      base64.StdEncoding.EncodeToString(sum),
		Name:          payload.Name,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetUsers(ctx *gin.Context) {
	var users []DB.User

	status := DB.DB.Find(&users)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []userPayload

	for _, rec := range users {
		response = append(response, userPayload{
			ID:            int(rec.ID),
			Account:       rec.Account,
			Password:      rec.Password,
			Name:          rec.Name,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateUser(ctx *gin.Context) {
	var payload userPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"name":         payload.Name,
		"password":     payload.Password,
	}

	result := DB.DB.Model(DB.User{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteUser(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var user DB.User

	DB.DB.First(&user, "id = ?", payload.ID)

	result := DB.DB.Model(DB.User{}).Where("id = ?", user.ID).Delete(DB.User{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

/*
 * Signin Flow:
 * 1. Check if the user is existed
 * 2. Hash password
 * (1. 2. Should be both computed, prevent side-channel attack)
 * 3. If user is existed, compare the password.
 */
func UserSignin(ctx *gin.Context) {
	var payload userSigninPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Extract User
	var user DB.User
	DB.DB.Where("account = ?", payload.Account).First(&user)

	// Hash Password
	hash := hmac.New(sha256.New, PasswordHashKey)
	hash.Write([]byte(payload.Password))
	sum := hash.Sum(nil)

	// Base64 Decode DB Data
	pass, err := base64.StdEncoding.DecodeString(user.Password)
	if err != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	}

	// Check if User is existed
	if user.Account == "" {
		ctx.String(http.StatusBadRequest, "Account or Password not Correct")
		return
	}

	if hmac.Equal(sum, pass) {
		// 密碼正確
		// 處理 JWT 的問題
		tokenString, err := Auth.JWTCreateNewToken(user)

		if err != nil {
			ctx.String(http.StatusInternalServerError, "Internal Server Error")
			return
		}

		ctx.JSON(http.StatusOK, struct {
			Token string `json:"token"`
		}{
			Token: tokenString,
		})
	} else {
		ctx.String(http.StatusBadRequest, "Account or Password not Correct")
		return
	}
}

func CheckJWTValid(ctx *gin.Context) {
	ctx.String(http.StatusOK, "ok")
}
