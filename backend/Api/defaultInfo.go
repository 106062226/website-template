package Api

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

type defaultInfoPayload struct {
	ID              int    `json:"id" binding:"required"`
	Link            string `json:"link" binding:"required"`
	LinkFileID      int    `json:"link_file_id" binding:"required"`
	Title           string `json:"title"`
	Contact         string `json:"contact"`
}

func CreateDefaultInfo(ctx *gin.Context) {
	var payload defaultInfoPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.DefaultInfo{
		Link:            payload.Link,
		LinkFileID:      payload.LinkFileID,
		Title:           payload.Title,
		Contact:         payload.Contact,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetDefaultInfos(ctx *gin.Context) {
	var defaultInfos []DB.DefaultInfo

	status := DB.DB.Find(&defaultInfos)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []defaultInfoPayload

	for _, rec := range defaultInfos {
		response = append(response, defaultInfoPayload{
			ID:              int(rec.ID),
			Link:            rec.Link,
			LinkFileID:      int(rec.LinkFileID),
			Title:           rec.Title,
			Contact:         rec.Contact,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateDefaultInfo(ctx *gin.Context) {
	var payload defaultInfoPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"link":              payload.Link,
		"link_file_id":      payload.LinkFileID,
		"title":             payload.Title,
		"contact":           payload.Contact,
	}

	result := DB.DB.Model(DB.DefaultInfo{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteDefaultInfo(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var defaultInfo DB.DefaultInfo
	var file DB.File

	DB.DB.First(&defaultInfo, "id = ?", payload.ID)

	if defaultInfo.Link == "" {
		ctx.String(http.StatusOK, "Ok")
		return
	}

	if defaultInfo.LinkFileID != -1 {
		DB.DB.First(&file, "id = ?", defaultInfo.LinkFileID)

		err := os.Remove(FileUploadPath + "/" + file.InternalFileName)
		result2 := DB.DB.Model(DB.File{}).Where("id = ?", file.ID).Delete(DB.File{})

		if result2.Error != nil || err != nil {
			ctx.String(http.StatusInternalServerError, "Internal Server Error")
			return
		}
	}

	result := DB.DB.Model(DB.DefaultInfo{}).Where("id = ?", defaultInfo.ID).Delete(DB.DefaultInfo{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
