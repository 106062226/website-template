package Api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

type teamPayload struct {
	ID              int    `json:"id" binding:"required"`
	Title           string `json:"title" binding:"required"`
	Schedule        string `json:"schedule" binding:"required"`
	Gather          string `json:"gather" binding:"required"`
	Host            string `json:"host" binding:"required"`
	Phone           string `json:"phone"`
	Extension       string `json:"extension"`
	Email           string `json:"email"`
}

func CreateTeam(ctx *gin.Context) {
	var payload teamPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.Team{
		Title:         payload.Title,
		Schedule:      payload.Schedule,
		Gather:        payload.Gather,
		Host:          payload.Host,
		Phone:         payload.Phone,
		Extension:     payload.Extension,
		Email:         payload.Email,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetTeams(ctx *gin.Context) {
	var teams []DB.Team

	status := DB.DB.Find(&teams)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []teamPayload

	for _, rec := range teams {
		response = append(response, teamPayload{
			ID:              int(rec.ID),
			Title:           rec.Title,
			Schedule:        rec.Schedule,
			Gather:          rec.Gather,
			Host:            rec.Host,
			Phone:           rec.Phone,
			Extension:       rec.Extension,
			Email:           rec.Email,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateTeam(ctx *gin.Context) {
	var payload teamPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"title":         payload.Title,
		"schedule":      payload.Schedule,
		"gather":        payload.Gather,
		"host":          payload.Host,
		"phone":         payload.Phone,
		"extension":     payload.Extension,
		"email":         payload.Email,
	}

	result := DB.DB.Model(DB.Team{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteTeam(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var team DB.Team
	// var file DB.File

	DB.DB.First(&team, "id = ?", payload.ID)

	// if team.Link == "" {
	// 	ctx.String(http.StatusOK, "Ok")
	// 	return
	// }

	// if team.LinkFileID != -1 {
	// 	DB.DB.First(&file, "id = ?", team.LinkFileID)

	// 	err := os.Remove(FileUploadPath + "/" + file.InternalFileName)
	// 	result2 := DB.DB.Model(DB.File{}).Where("id = ?", file.ID).Delete(DB.File{})

	// 	if result2.Error != nil || err != nil {
	// 		ctx.String(http.StatusInternalServerError, "Internal Server Error")
	// 		return
	// 	}
	// }

	result := DB.DB.Model(DB.Team{}).Where("id = ?", team.ID).Delete(DB.Team{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
