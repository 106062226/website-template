package Api

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

type targetPayload struct {
	ID              int    `json:"id" binding:"required"`
	Link            string `json:"link" binding:"required"`
	LinkFileID      int    `json:"link_file_id" binding:"required"`
	Intro           string `json:"intro"`
}

func CreateTarget(ctx *gin.Context) {
	var payload targetPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.Target{
		Link:            payload.Link,
		LinkFileID:      payload.LinkFileID,
		Intro:           payload.Intro,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetTargets(ctx *gin.Context) {
	var targets []DB.Target

	status := DB.DB.Find(&targets)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []targetPayload

	for _, rec := range targets {
		response = append(response, targetPayload{
			ID:              int(rec.ID),
			Link:            rec.Link,
			LinkFileID:      int(rec.LinkFileID),
			Intro:           rec.Intro,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateTarget(ctx *gin.Context) {
	var payload targetPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"link":              payload.Link,
		"link_file_id":      payload.LinkFileID,
		"intro":             payload.Intro,
	}

	result := DB.DB.Model(DB.Target{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteTarget(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var target DB.Target
	var file DB.File

	DB.DB.First(&target, "id = ?", payload.ID)

	if target.Link == "" {
		ctx.String(http.StatusOK, "Ok")
		return
	}

	if target.LinkFileID != -1 {
		DB.DB.First(&file, "id = ?", target.LinkFileID)

		err := os.Remove(FileUploadPath + "/" + file.InternalFileName)
		result2 := DB.DB.Model(DB.File{}).Where("id = ?", file.ID).Delete(DB.File{})

		if result2.Error != nil || err != nil {
			ctx.String(http.StatusInternalServerError, "Internal Server Error")
			return
		}
	}

	result := DB.DB.Model(DB.Target{}).Where("id = ?", target.ID).Delete(DB.Target{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
