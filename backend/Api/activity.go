package Api

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

// Payload
type activityPayload struct {
	ID              int    `json:"id" binding:"required"`
	Title           string `json:"title"  binding:"required"`
	Description     string `json:"desc" binding:"required"`
	AuthorName      string `json:"author_name"`
	AuthorTitle     string `json:"author_title"`
	Time            string `json:"time"  binding:"required"`
	Location        string `json:"location" binding:"required"`
	SeatLimit       int    `json:"seat_limit"  binding:"required"`
	Link            string `json:"link"`
	Thumbnail       string `json:"thumbnail"`
	ThumbnailFileID int    `json:"thumbnail_file_id"`
	ContactName     string `json:"contact_name"`
	ContactPhone    string `json:"contact_phone"`
}

func CreateActivity(ctx *gin.Context) {
	var payload activityPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.Activity{
		Title:           payload.Title,
		Description:     payload.Description,
		AuthorName:      payload.AuthorName,
		AuthorTitle:     payload.AuthorTitle,
		Time:            payload.Time,
		Location:        payload.Location,
		SeatLimit:       uint(payload.SeatLimit),
		Link:            payload.Link,
		Thumbnail:       payload.Thumbnail,
		ThumbnailFileID: payload.ThumbnailFileID,
		ContactName:     payload.ContactName,
		ContactPhone:    payload.ContactPhone,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetActivities(ctx *gin.Context) {
	var activities []DB.Activity

	status := DB.DB.Find(&activities)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []activityPayload

	for _, rec := range activities {
		response = append(response, activityPayload{
			ID:              int(rec.ID),
			Title:           rec.Title,
			Description:     rec.Description,
			AuthorName:      rec.AuthorName,
			AuthorTitle:     rec.AuthorTitle,
			Time:            rec.Time,
			Location:        rec.Location,
			SeatLimit:       int(rec.SeatLimit),
			Link:            rec.Link,
			Thumbnail:       rec.Thumbnail,
			ThumbnailFileID: int(rec.ThumbnailFileID),
			ContactName:     rec.ContactName,
			ContactPhone:    rec.ContactPhone,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateActivity(ctx *gin.Context) {
	var payload activityPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"title":             payload.Title,
		"description":       payload.Description,
		"author_name":       payload.AuthorName,
		"author_title":      payload.AuthorTitle,
		"time":              payload.Time,
		"location":          payload.Location,
		"seat_limit":        uint(payload.SeatLimit),
		"link":              payload.Link,
		"thumbnail":         payload.Thumbnail,
		"thumbnail_file_id": payload.ThumbnailFileID,
		"contact_name":      payload.ContactName,
		"contact_phone":     payload.ContactPhone,
	}

	result := DB.DB.Model(DB.Activity{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteActivity(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var activity DB.Activity
	var file DB.File

	DB.DB.First(&activity, "id = ?", payload.ID)

	if activity.Title == "" {
		ctx.String(http.StatusOK, "Ok")
		return
	}

	if activity.ThumbnailFileID != -1 {
		DB.DB.First(&file, "id = ?", activity.ThumbnailFileID)

		err := os.Remove(FileUploadPath + "/" + file.InternalFileName)
		result2 := DB.DB.Model(DB.File{}).Where("id = ?", file.ID).Delete(DB.File{})

		if result2.Error != nil || err != nil {
			ctx.String(http.StatusInternalServerError, "Internal Server Error")
			return
		}
	}

	result := DB.DB.Model(DB.Activity{}).Where("id = ?", activity.ID).Delete(DB.Activity{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
