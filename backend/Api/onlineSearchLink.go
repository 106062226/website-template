package Api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

// Payload
type onlineSearchLinkPayload struct {
	ID    int    `json:"id" binding:"required"`
	Title string `json:"title"  binding:"required"`
	Link  string `json:"link"  binding:"required"`
}

func CreateOnlineSearchLink(ctx *gin.Context) {
	var payload onlineSearchLinkPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.OnlineSearchLink{
		Title: payload.Title,
		Link:  payload.Link,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetOnlineSearchLinks(ctx *gin.Context) {
	var onlineSearchLinks []DB.OnlineSearchLink

	status := DB.DB.Find(&onlineSearchLinks)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []onlineSearchLinkPayload

	for _, rec := range onlineSearchLinks {
		response = append(response, onlineSearchLinkPayload{
			ID:    int(rec.ID),
			Title: rec.Title,
			Link:  rec.Link,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateOnlineSearchLink(ctx *gin.Context) {
	var payload onlineSearchLinkPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"title": payload.Title,
		"link":  payload.Link,
	}

	result := DB.DB.Model(DB.OnlineSearchLink{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteOnlineSearchLink(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	result := DB.DB.Model(DB.OnlineSearchLink{}).Where("id = ?", payload.ID).Delete(DB.OnlineSearchLink{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
