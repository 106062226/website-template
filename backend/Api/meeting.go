package Api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

// Payload
type meetingPayload struct {
	ID    int    `json:"id" binding:"required"`
	Title string `json:"title"  binding:"required"`
	Time  string `json:"time"  binding:"required"`
	Place string `json:"place" binding:"required"`
}

func CreateMeeting(ctx *gin.Context) {
	var payload meetingPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.Meeting{
		Title: payload.Title,
		Time:  payload.Time,
		Place: payload.Place,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetMeetings(ctx *gin.Context) {
	var meetings []DB.Meeting

	status := DB.DB.Find(&meetings)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []meetingPayload

	for _, rec := range meetings {
		response = append(response, meetingPayload{
			ID:    int(rec.ID),
			Title: rec.Title,
			Time:  rec.Time,
			Place: rec.Place,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateMeeting(ctx *gin.Context) {
	var payload meetingPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"title": payload.Title,
		"time":  payload.Time,
		"place": payload.Place,
	}

	result := DB.DB.Model(DB.Meeting{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteMeeting(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	result := DB.DB.Model(DB.Meeting{}).Where("id = ?", payload.ID).Delete(DB.Meeting{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
