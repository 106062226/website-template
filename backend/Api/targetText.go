package Api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

type targetTextPayload struct {
	ID              int    `json:"id"`
	Target          string `json:"target"`
}

func CreateTargetText(ctx *gin.Context) {
	var payload targetTextPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.TargetText{
		Target:          payload.Target,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetTargetTexts(ctx *gin.Context) {
	var targetTexts []DB.TargetText

	status := DB.DB.Find(&targetTexts)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []targetTextPayload

	for _, rec := range targetTexts {
		response = append(response, targetTextPayload{
			ID:              int(rec.ID),
			Target:          rec.Target,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateTargetText(ctx *gin.Context) {
	var payload targetTextPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"target":            payload.Target,
	}

	result := DB.DB.Model(DB.TargetText{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteTargetText(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var targetText DB.TargetText
	DB.DB.First(&targetText, "id = ?", payload.ID)

	result := DB.DB.Model(DB.TargetText{}).Where("id = ?", targetText.ID).Delete(DB.TargetText{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
