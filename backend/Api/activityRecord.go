package Api

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

// Payload
type activityRecordPayload struct {
	ID           int    `json:"id" binding:"required"`
	Type         string `json:"type" binding:"required"`
	Title        string `json:"title"  binding:"required"`
	Author       string `json:"author"`
	Location     string `json:"location" binding:"required"`
	Time         string `json:"time" binding:"required"`
	Book         string `json:"book"`
	Content      string `json:"content" binding:"required"`
	ImageLink1   string `json:"image_link1" binding:"required"`
	ImageFileId1 int    `json:"image_file_id1" binding:"required"`
	ImageLink2   string `json:"image_link2" binding:"required"`
	ImageFileId2 int    `json:"image_file_id2" binding:"required"`
}

func CreateActivityRecord(ctx *gin.Context) {
	var payload activityRecordPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.ActivityRecord{
		Type:         payload.Type,
		Title:        payload.Title,
		Author:       payload.Author,
		Location:     payload.Location,
		Time:         payload.Time,
		Book:         payload.Book,
		Content:      payload.Content,
		ImageLink1:   payload.ImageLink1,
		ImageFileID1: payload.ImageFileId1,
		ImageLink2:   payload.ImageLink2,
		ImageFileID2: payload.ImageFileId2,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetActivityRecords(ctx *gin.Context) {
	var activityRecords []DB.ActivityRecord

	status := DB.DB.Find(&activityRecords)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []activityRecordPayload

	for _, rec := range activityRecords {
		response = append(response, activityRecordPayload{
			ID:           int(rec.ID),
			Type:         rec.Type,
			Title:        rec.Title,
			Author:       rec.Author,
			Location:     rec.Location,
			Time:         rec.Time,
			Book:         rec.Book,
			Content:      rec.Content,
			ImageLink1:   rec.ImageLink1,
			ImageFileId1: int(rec.ImageFileID1),
			ImageLink2:   rec.ImageLink2,
			ImageFileId2: int(rec.ImageFileID2),
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateActivityRecord(ctx *gin.Context) {
	var payload activityRecordPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"type":           payload.Type,
		"title":          payload.Title,
		"author":         payload.Author,
		"location":       payload.Location,
		"time":           payload.Time,
		"book":           payload.Book,
		"content":        payload.Content,
		"image_link1":    payload.ImageLink1,
		"image_file_id1": payload.ImageFileId1,
		"image_link2":    payload.ImageLink2,
		"image_file_id2": payload.ImageFileId2,
	}

	result := DB.DB.Model(DB.ActivityRecord{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteActivityRecord(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var activityRecord DB.ActivityRecord
	var file1, file2 DB.File

	DB.DB.First(&activityRecord, "id = ?", payload.ID)

	if activityRecord.Title == "" {
		ctx.String(http.StatusOK, "Ok")
		return
	}

	if activityRecord.ImageFileID1 != -1 {
		DB.DB.First(&file1, "id = ?", activityRecord.ImageFileID1)

		err := os.Remove(FileUploadPath + "/" + file1.InternalFileName)
		result2 := DB.DB.Model(DB.File{}).Where("id = ?", activityRecord.ImageFileID1).Delete(DB.File{})

		if result2.Error != nil || err != nil {
			ctx.String(http.StatusInternalServerError, "Internal Server Error")
			return
		}
	}

	if activityRecord.ImageFileID2 != -1 {
		DB.DB.First(&file2, "id = ?", activityRecord.ImageFileID2)

		err := os.Remove(FileUploadPath + "/" + file2.InternalFileName)
		result3 := DB.DB.Model(DB.File{}).Where("id = ?", activityRecord.ImageFileID2).Delete(DB.File{})

		if result3.Error != nil || err != nil {
			ctx.String(http.StatusInternalServerError, "Internal Server Error")
			return
		}
	}

	result := DB.DB.Model(DB.ActivityRecord{}).Where("id = ?", activityRecord.ID).Delete(DB.ActivityRecord{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
