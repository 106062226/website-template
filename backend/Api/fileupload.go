package Api

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/cs-task-force/server/DB"
)

type filePayload struct {
	ID               int    `json:"id" binding:"required"`
	Name             string `json:"name"  binding:"required"`
	InternalFileName string `json:"internal_file_name"  binding:"required"`
	Link             string `json:"link" binding:"required"`
	Type             string `json:"type" binding:"required"`
}

type publicFilePayload struct {
	Name string `json:"name"  binding:"required"`
	Link string `json:"link" binding:"required"`
}

func FileUpload(ctx *gin.Context) {
	// Accepted File Format: docx, jpg, png
	file, _ := ctx.FormFile("file")

	// 從 Filename 來判斷檔案型別真的是滿爛的，基本上很沒用，但
	// 我想其他辦法就交給下個人來維護吧 XD (簡單防呆就好)
	// if !strings.HasSuffix(file.Filename, ".docx") && !strings.HasSuffix(file.Filename, ".jpg") && !strings.HasSuffix(file.Filename, ".png") && !strings.HasSuffix(file.Filename, ".jpeg") {
	// 	ctx.String(http.StatusBadRequest, "Filetype not Allowed")
	// 	return
	// }

	// 這邊 File Name 直接不檢查了，他們完全沒有說支援什麼格式，被攻擊就被攻擊吧 XD

	fileUUID := uuid.NewV4()

	// Extract File type
	fileNameSegment := strings.Split(file.Filename, ".")
	// 直接不 Filter 了

	newFileName := fileUUID.String() + "." + fileNameSegment[len(fileNameSegment)-1]

	err := ctx.SaveUploadedFile(file, FileUploadPath+"/"+newFileName)

	if err != nil {
		fmt.Println(err)
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	}

	fileLink := strings.TrimLeft(FileUploadPath, "./") + "/" + newFileName

	// Update to DB
	dbPayload := DB.File{
		Name:             file.Filename,
		InternalFileName: newFileName,
		Link:             fileLink,
		Type:             "Assets",
	}

	result := DB.DB.Create(&dbPayload)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	}

	ctx.JSON(http.StatusAccepted, struct {
		Link string `json:"link"`
		ID   int    `json:"id"`
	}{
		Link: fileLink,
		ID:   int(dbPayload.ID),
	})
}

func GetFiles(ctx *gin.Context) {
	var files []DB.File

	status := DB.DB.Where("type != ?", "Assets").Find(&files)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []filePayload

	for _, rec := range files {
		response = append(response, filePayload{
			ID:               int(rec.ID),
			Name:             rec.Name,
			InternalFileName: rec.InternalFileName,
			Link:             rec.Link,
			Type:             rec.Type,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func GetPublicFiles(ctx *gin.Context) {
	var files []DB.File

	status := DB.DB.Where("type != ?", "Assets").Find(&files)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []publicFilePayload

	for _, rec := range files {
		response = append(response, publicFilePayload{
			Name: rec.Name,
			Link: rec.Link,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateFileType(ctx *gin.Context) {
	var payload struct {
		Type string `json:"type" binding:"required"`
		ID   int    `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	result := DB.DB.Model(DB.File{}).Where("id = ?", payload.ID).Update("type", payload.Type)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteFile(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var file DB.File

	DB.DB.First(&file, "id = ?", payload.ID)

	if file.Name == "" {
		ctx.String(http.StatusOK, "File not found")
		return
	}

	result := DB.DB.Model(DB.File{}).Where("id = ?", file.ID).Delete(DB.File{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	}

	err := os.Remove(FileUploadPath + "/" + file.InternalFileName)

	if err != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
