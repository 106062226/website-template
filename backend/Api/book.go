package Api

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

// Payload
type bookPayload struct {
	ID          int    `json:"id" binding:"required"`
	Type        string `json:"type" binding:"required"`
	Name        string `json:"name"  binding:"required"`
	Author      string `json:"author"  binding:"required"`
	Publication string `json:"publication" binding:"required"`
	Intro       string `json:"intro" binding:"required"`
	ImageLink   string `json:"image_link" binding:"required"`
	ImageFileId int    `json:"image_file_id" binding:"required"`
}

func CreateBook(ctx *gin.Context) {
	var payload bookPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.Book{
		Type:        payload.Type,
		Name:        payload.Name,
		Author:      payload.Author,
		Publication: payload.Publication,
		Intro:       payload.Intro,
		ImageLink:   payload.ImageLink,
		ImageFileID: payload.ImageFileId,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetBooks(ctx *gin.Context) {
	var books []DB.Book

	status := DB.DB.Find(&books)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []bookPayload

	for _, rec := range books {
		response = append(response, bookPayload{
			ID:          int(rec.ID),
			Type:        rec.Type,
			Name:        rec.Name,
			Author:      rec.Author,
			Publication: rec.Publication,
			Intro:       rec.Intro,
			ImageLink:   rec.ImageLink,
			ImageFileId: int(rec.ImageFileID),
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateBook(ctx *gin.Context) {
	var payload bookPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"type":          payload.Type,
		"name":          payload.Name,
		"author":        payload.Author,
		"publication":   payload.Publication,
		"intro":         payload.Intro,
		"image_link":    payload.ImageLink,
		"image_file_id": payload.ImageFileId,
	}

	result := DB.DB.Model(DB.Book{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteBook(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var book DB.Book
	var file DB.File

	DB.DB.First(&book, "id = ?", payload.ID)

	if book.Name == "" {
		ctx.String(http.StatusOK, "Ok")
		return
	}

	if book.ImageFileID != -1 {
		DB.DB.First(&file, "id = ?", book.ImageFileID)

		err := os.Remove(FileUploadPath + "/" + file.InternalFileName)
		result2 := DB.DB.Model(DB.File{}).Where("id = ?", book.ImageFileID).Delete(DB.File{})

		if result2.Error != nil || err != nil {
			ctx.String(http.StatusInternalServerError, "Internal Server Error")
			return
		}
	}

	result := DB.DB.Model(DB.Book{}).Where("id = ?", book.ID).Delete(DB.Book{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
