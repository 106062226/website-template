package Api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

type memberPayload struct {
	ID              int    `json:"id" binding:"required"`
	Name            string `json:"name" binding:"required"`
	Role            string `json:"role"`
	Host            string `json:"host"`
	District        string `json:"district"`
	Team            int    `json:"team" binding:"required"`
	Phone           string `json:"phone"`
	Extension       string `json:"extension"`
	Email           string `json:"email"`
}

func CreateMember(ctx *gin.Context) {
	var payload memberPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.Member{
		Name:         payload.Name,
		Role:         payload.Role,
		Host:         payload.Host,
		District:     payload.District,
		Team:         payload.Team,
		Phone:        payload.Phone,
		Extension:    payload.Extension,
		Email:        payload.Email,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetMembers(ctx *gin.Context) {
	var members []DB.Member

	status := DB.DB.Find(&members)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []memberPayload

	for _, rec := range members {
		response = append(response, memberPayload{
			ID:           int(rec.ID),
			Name:         rec.Name,
			Role:         rec.Role,
			Host:         rec.Host,
			District:     rec.District,
			Team:         int(rec.Team),
			Phone:        rec.Phone,
			Extension:    rec.Extension,
			Email:        rec.Email,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateMember(ctx *gin.Context) {
	var payload memberPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"name":         payload.Name,
		"role":         payload.Role,
		"host":         payload.Host,
		"district":     payload.District,
		"team":         payload.Team,
		"phone":        payload.Phone,
		"extension":    payload.Extension,
		"email":        payload.Email,
	}

	result := DB.DB.Model(DB.Member{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteMember(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var member DB.Member
	// var file DB.File

	DB.DB.First(&member, "id = ?", payload.ID)

	// if member.Link == "" {
	// 	ctx.String(http.StatusOK, "Ok")
	// 	return
	// }

	// if member.LinkFileID != -1 {
	// 	DB.DB.First(&file, "id = ?", member.LinkFileID)

	// 	err := os.Remove(FileUploadPath + "/" + file.InternalFileName)
	// 	result2 := DB.DB.Model(DB.File{}).Where("id = ?", file.ID).Delete(DB.File{})

	// 	if result2.Error != nil || err != nil {
	// 		ctx.String(http.StatusInternalServerError, "Internal Server Error")
	// 		return
	// 	}
	// }

	result := DB.DB.Model(DB.Member{}).Where("id = ?", member.ID).Delete(DB.Member{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
