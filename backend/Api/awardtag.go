package Api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

type awardtagPayload struct {
	ID              int    `json:"id" binding:"required"`
	Title           string `json:"title" binding:"required"`
}

func CreateAwardtag(ctx *gin.Context) {
	var payload awardtagPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.Awardtag{
		Title:         payload.Title,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetAwardtags(ctx *gin.Context) {
	var awardtags []DB.Awardtag

	status := DB.DB.Find(&awardtags)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []awardtagPayload

	for _, rec := range awardtags {
		response = append(response, awardtagPayload{
			ID:              int(rec.ID),
			Title:           rec.Title,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateAwardtag(ctx *gin.Context) {
	var payload awardtagPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"title":         payload.Title,
	}

	result := DB.DB.Model(DB.Awardtag{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteAwardtag(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var awardtag DB.Awardtag
	// var file DB.File

	DB.DB.First(&awardtag, "id = ?", payload.ID)

	// if awardtag.Link == "" {
	// 	ctx.String(http.StatusOK, "Ok")
	// 	return
	// }

	// if awardtag.LinkFileID != -1 {
	// 	DB.DB.First(&file, "id = ?", awardtag.LinkFileID)

	// 	err := os.Remove(FileUploadPath + "/" + file.InternalFileName)
	// 	result2 := DB.DB.Model(DB.File{}).Where("id = ?", file.ID).Delete(DB.File{})

	// 	if result2.Error != nil || err != nil {
	// 		ctx.String(http.StatusInternalServerError, "Internal Server Error")
	// 		return
	// 	}
	// }

	result := DB.DB.Model(DB.Awardtag{}).Where("id = ?", awardtag.ID).Delete(DB.Awardtag{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
