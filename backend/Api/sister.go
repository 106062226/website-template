package Api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/DB"
)

// Payload
type sisterPayload struct {
	ID              int    `json:"id" binding:"required"`
	Name            string `json:"name"  binding:"required"`
	NameEn          string `json:"name_en"  binding:"required"`
	NameSimple      string `json:"name_simple"`
	Country         string `json:"country"  binding:"required"`
	CountryEn       string `json:"country_en"  binding:"required"`
	Area            string `json:"area"  binding:"required"`
	AreaEn          string `json:"area_en"  binding:"required"`
	Intro           string `json:"intro"`
	IntroEn         string `json:"intro_en"`
	IsSister        bool   `json:"is_sister" binding:"required"`
	ContractStart   string `json:"contract_start" binding:"required"`
	Website         string `json:"website"`
	Address         string `json:"address"`
	Phone           string `json:"phone"`
}

func CreateSister(ctx *gin.Context) {
	var payload sisterPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := DB.Sister{
		Name:          payload.Name,
		NameEn:     payload.NameEn,
		NameSimple:      payload.NameSimple,
		Country:     payload.Country,
		CountryEn:            payload.CountryEn,
		Area:        payload.Area,
		AreaEn:       payload.AreaEn,
		Intro:            payload.Intro,
		IntroEn:       payload.IntroEn,
		IsSister: payload.IsSister,
		ContractStart:     payload.ContractStart,
		Website:    payload.Website,
		Address:    payload.Address,
		Phone:    payload.Phone,
	}

	result := DB.DB.Create(&data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func GetSisters(ctx *gin.Context) {
	var sisters []DB.Sister

	status := DB.DB.Find(&sisters)

	if status.Error != nil {
		ctx.String(http.StatusInternalServerError, "Server Internal Error")
		return
	}

	var response []sisterPayload

	for _, rec := range sisters {
		response = append(response, sisterPayload{
			ID:              int(rec.ID),
			Name:           rec.Name,
			NameEn:     rec.NameEn,
			NameSimple:      rec.NameSimple,
			Country:     rec.Country,
			CountryEn:            rec.CountryEn,
			Area:        rec.Area,
			AreaEn:       rec.AreaEn,
			Intro:            rec.Intro,
			IntroEn:       rec.IntroEn,
			IsSister:     rec.IsSister,
			ContractStart:     rec.ContractStart,
			Website:    rec.Website,
			Address:     rec.Address,
			Phone:    rec.Phone,
		})
	}

	ctx.JSON(http.StatusOK, response)
}

func UpdateSister(ctx *gin.Context) {
	var payload sisterPayload

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	// Prepare Payload
	data := map[string]interface{}{
		"name":             payload.Name,
		"name_en":       payload.NameEn,
		"name_simple":       payload.NameSimple,
		"country":      payload.Country,
		"country_en":              payload.CountryEn,
		"area":          payload.Area,
		"area_en":        payload.AreaEn,
		"intro":              payload.Intro,
		"intro_en":         payload.IntroEn,
		"is_sister": payload.IsSister,
		"contract_start":      payload.ContractStart,
		"website":     payload.Website,
		"phone":      payload.Phone,
		"address":     payload.Address,
	}

	result := DB.DB.Model(DB.Sister{}).Where("id = ?", payload.ID).Updates(data)

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}

func DeleteSister(ctx *gin.Context) {
	var payload struct {
		ID int `json:"id" binding:"required"`
	}

	// Validator & Extract Json Data
	if err := ctx.ShouldBindJSON(&payload); err != nil {
		ctx.String(http.StatusBadRequest, "Bad Request")
		return
	}

	var sister DB.Sister

	DB.DB.First(&sister, "id = ?", payload.ID)

	if sister.Name == "" {
		ctx.String(http.StatusOK, "Ok")
		return
	}

	result := DB.DB.Model(DB.Sister{}).Where("id = ?", sister.ID).Delete(DB.Sister{})

	if result.Error != nil {
		ctx.String(http.StatusInternalServerError, "Internal Server Error")
		return
	} else {
		ctx.String(http.StatusOK, "Ok")
	}
}
