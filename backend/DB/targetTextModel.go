package DB

type TargetText struct {
	ID          uint `gorm:"primaryKey;autoIncrement"`
	Target      string
}

func initTargetText() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&TargetText{}) {
		DB.Migrator().CreateTable(&TargetText{})
	}
}
