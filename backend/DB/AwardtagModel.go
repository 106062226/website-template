package DB

type Awardtag struct {
	ID             uint `gorm:"primaryKey;autoIncrement"`
	Title          string
}

func initAwardtag() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&Awardtag{}) {
		DB.Migrator().CreateTable(&Awardtag{})
	}
}
