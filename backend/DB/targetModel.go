package DB

type Target struct {
	ID          uint `gorm:"primaryKey;autoIncrement"`
	Link        string
	LinkFileID  int
	Intro       string
}

func initTarget() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&Target{}) {
		DB.Migrator().CreateTable(&Target{})
	}
}
