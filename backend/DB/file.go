package DB

import "time"

type File struct {
	ID               uint `gorm:"primaryKey;autoIncrement"`
	Name             string
	InternalFileName string
	Link             string
	Type             string
	CreatedAt        time.Time
	UpdatedAt        time.Time
}

func initFile() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&File{}) {
		DB.Migrator().CreateTable(&File{})
	}
}
