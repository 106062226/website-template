package DB

import "time"

type Sister struct {
	ID              uint `gorm:"primaryKey;autoIncrement"`
	Name            string
	NameEn          string
	NameSimple      string
	Country      string
	CountryEn     string
	Area            string
	AreaEn        string
	Intro       string
	IntroEn            string
	IsSister       bool
	ContractStart string
	Phone     string
	Address     string
	Website     string
	ContactPhone    string
	CreatedAt       time.Time
	UpdatedAt       time.Time
}

func initSister() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&Sister{}) {
		DB.Migrator().CreateTable(&Sister{})
	}
}
