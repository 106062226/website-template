package DB

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

// Global Database Driver
var DB *gorm.DB

func Init() {
	db, err := gorm.Open(sqlite.Open("database.db"), &gorm.Config{})
	if err != nil {
		panic("Error ermerge when open database file.")
	}

	// Assign to DB
	DB = db

	// Init each Model
	initUser()
	initActivity()
	initMeeting()
	initBook()
	initTarget()
	initTargetText()
	initDefaultInfo()
	initTeam()
	initMember()
	initActivityRecord()
	initFile()
	initOnlineSearchLink()
	initAward()
	initAwardtag()

	initSister()
}
