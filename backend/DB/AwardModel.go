package DB

type Award struct {
	ID             uint `gorm:"primaryKey;autoIncrement"`
	Title          string
	Identity       string
	Competition    string
	Award          string
	Tag            int
}

func initAward() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&Award{}) {
		DB.Migrator().CreateTable(&Award{})
	}
}
