package DB

type Team struct {
	ID             uint `gorm:"primaryKey;autoIncrement"`
	Title          string
	Schedule       string
	Gather         string
	Host           string
	Phone      string
	Extension  string
	Email      string
}

func initTeam() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&Team{}) {
		DB.Migrator().CreateTable(&Team{})
	}
}
