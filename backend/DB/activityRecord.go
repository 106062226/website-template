package DB

import "time"

type ActivityRecord struct {
	ID           uint `gorm:"primaryKey;autoIncrement"`
	Type         string
	Title        string
	Author       string
	Location     string
	Time         string
	Book         string
	Content      string
	ImageLink1   string
	ImageFileID1 int
	ImageLink2   string
	ImageFileID2 int
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

func initActivityRecord() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&ActivityRecord{}) {
		DB.Migrator().CreateTable(&ActivityRecord{})
	}
}
