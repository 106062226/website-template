package DB

type DefaultInfo struct {
	ID          uint `gorm:"primaryKey;autoIncrement"`
	Link        string
	LinkFileID  int
	Title       string
	Contact     string
}

func initDefaultInfo() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&DefaultInfo{}) {
		DB.Migrator().CreateTable(&DefaultInfo{})
	}
}
