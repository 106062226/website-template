package DB

import "time"

type Meeting struct {
	ID        uint `gorm:"primaryKey;autoIncrement"`
	Title     string
	Time      string
	Place     string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func initMeeting() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&Meeting{}) {
		DB.Migrator().CreateTable(&Meeting{})
	}
}
