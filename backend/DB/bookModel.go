package DB

import "time"

type Book struct {
	ID          uint `gorm:"primaryKey;autoIncrement"`
	Type        string
	Name        string
	Author      string
	Publication string
	Intro       string
	ImageLink   string
	ImageFileID int
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

func initBook() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&Book{}) {
		DB.Migrator().CreateTable(&Book{})
	}
}
