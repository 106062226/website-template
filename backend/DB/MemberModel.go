package DB

type Member struct {
	ID          uint `gorm:"primaryKey;autoIncrement"`
	Name        string
	Role        string
	Host        string
	District    string
	Team        int
	Phone       string
	Extension   string
	Email       string
}

func initMember() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&Member{}) {
		DB.Migrator().CreateTable(&Member{})
	}
}
