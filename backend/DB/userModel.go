package DB

type User struct {
	ID       uint `gorm:"primaryKey;autoIncrement"`
	Account  string
	Password string
	Name     string
}

func initUser() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&User{}) {
		DB.Migrator().CreateTable(&User{})
	}
}
