package DB

import "time"

type OnlineSearchLink struct {
	ID        uint `gorm:"primaryKey;autoIncrement"`
	Title     string
	Link      string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func initOnlineSearchLink() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&OnlineSearchLink{}) {
		DB.Migrator().CreateTable(&OnlineSearchLink{})
	}
}
