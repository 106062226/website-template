package DB

import "time"

type Activity struct {
	ID              uint `gorm:"primaryKey;autoIncrement"`
	Title           string
	Description     string
	AuthorName      string
	AuthorTitle     string
	Time            string
	Location        string
	SeatLimit       uint
	Link            string
	Thumbnail       string
	ThumbnailFileID int
	ContactName     string
	ContactPhone    string
	CreatedAt       time.Time
	UpdatedAt       time.Time
}

func initActivity() {
	// Check if Has Table. If not, create one.
	if !DB.Migrator().HasTable(&Activity{}) {
		DB.Migrator().CreateTable(&Activity{})
	}
}
