import argparse
import hmac
import sqlite3
import csv
from base64 import b64decode, b64encode
from hashlib import sha256
from textwrap import dedent

Keys = {}

def hash_password(password):
    digest = hmac.digest(b64decode(Keys["PASSWORD_HMAC_KEY"]), password.encode(), sha256)

    return b64encode(digest).decode("utf8")

def simple_env_parse():
    with open("key.env", "r") as f:
        lines = f.readlines()

        for line in lines:
            if line[0] == '#':
                continue
            pair_index = line.find("=")
            Keys[line[0:pair_index]] = line[pair_index+1:]
        
        f.close()

class UserAction:

    def __init__(self, db) -> None:
        print(f"Connecting to {db}...")
        self.con = sqlite3.connect(db)


    def __del__(self):
        self.con.commit()
        self.con.close()

    def Create(self, account, password, name):
        cur = self.con.cursor()

        cur.execute(f'INSERT INTO users (`account`, `password`, `name`) VALUES ("{account}", "{hash_password(password)}", "{name}")')

    def BulkCreate(self, csv_filename):
        with open(csv_filename, "r") as f:
            reader = csv.reader(f, delimiter=' ', quotechar='|')

            for row in list(reader)[1:]:
                data = row[0].split(",")
                self.Create(data[1], data[2], data[0])
            
            f.close()

    def Read(self, account):
        cur = self.con.cursor()

        data = cur.execute(f'SELECT * FROM users WHERE account = "{account}"')

        for row in data:
            print(row)

    def Update(self, account, password):
        cur = self.con.cursor()

        cur.execute(f'UPDATE users SET password = "{password}" WHERE account = "{account}"')

    def Delete(self, account):
        cur = self.con.cursor()

        cur.execute(f'DELETE FROM users WHERE account = "{account}"')
        pass


def arg_parser_setup():
    parser = argparse.ArgumentParser(
        description=dedent("""
            處理 User 的 CRUD
            \---------------------------------
                考量到安全性的問題，在主伺服器的程式碼裡我並沒有開出對 users Table
                相關的 API，所以使用者的 CRUD 都會透過這個 Script 處理
            \---------------------------------
        """),
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser.add_argument(
        "database",
        nargs=1,
        help="Sqlite3 DB 的名稱",
        metavar="Database Name"
    )

    parser.add_argument(
        "action",
        nargs=1,
        help="要執行的動作，可以是 create | bulkcreate | read | update | delete",
        choices=["create", "bulkcreate", "read", "update", "delete"],
        metavar="Action"
    )

    return vars(parser.parse_args())

if __name__ == "__main__":
    simple_env_parse()
    args = arg_parser_setup()

    userdb = UserAction(args["database"][0])

    # Action
    if args['action'][0] == "create":
        account = input("輸入所選帳號：")
        password = input("輸入密碼：")
        name = input("輸入帳戶名稱：")

        userdb.Create(account, password, name)
    elif args['action'][0] == "bulkcreate":
        filename = input("輸入 csv 檔案名稱：")

        userdb.BulkCreate(filename)
    elif args['action'][0] == "read":
        account = input("輸入帳號：")

        userdb.Read(account)
    elif args['action'][0] == "update":
        account = input("輸入要更新的帳號：")
        password = input("輸入新密碼：")

        userdb.Update(account, password)
    elif args['action'][0] == "delete":
        account = input("輸入要刪除的帳號：")

        userdb.Delete(account)
    else:
        pass