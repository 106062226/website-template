# 人事處 讀書專案 Backend Doc

人事處讀書頁面。總共分以下幾個部份要實做:

- Authentication
- Activity Infos & Results CRUD
- Document(static file like pdf, img....) Upload System
- Book Recommendation
- 文官學院頁面 Crawler

## Authentication

- Token Based or Session Based(之前是用 JWT，現在不知道主流是啥，可以去看一下要不要改技術)
- Password Login Only（估計是不太需要做 Third-Payty Login）
- Website Workflow Based on Token, Not In-Memory Variable

## Database

- Postgres 好了，畢竟沒用過，試試看
- 可以使用 [gorm](https://gorm.io/)

### 安裝時給的相關資訊

| Ver | Cluster | Port | Status | Owner | Data directory | Log file |
| --- | ------- | ---- | ------ | ----- | -------------- | -------- |
| 12  | main    | 5432 | down   | postgres | /var/lib/postgresql/12/main | /var/log/postgresql/postgresql-12-main.log |

啟動系統指令：

```zsh
# Usage: /usr/bin/pg_ctlcluster <version> <cluster> <action> [-- <pg_ctl options>]
pg_ctlcluster 12 main start

# 但是用 System Service 會好一點
service --status-all | grep postgresql

# 沒啟用的話可以啟用一下
sudo service postgresql start

# 不行的話再用 systemctl 吧
```

要先把自己加到 `postgres` 這個 group 中：

```zsh
sudo usermod -a -G postgres [username]
# 後來發現好像不用 = =
```

### psql 使用前置作業

在用 `psql` 前，有幾種方法可以使用這個 Command：

```zsh
# 1. 登入 postgres 這個 User
sudo -i -u postgres

# 2. 不登入，但每次都要使用 postgres 的名義執行
sudo -u postgres psql

# 3. 直接新增一個跟目前帳號一樣名字的 User 到 Postgres Server 上去
sudo -u postgres createuser --interactive
```

### 連接到 DB 去

```zsh
psql testing-db
```

## Apache

Ubuntu（or Dabian based distro）上的 Config 檔案是放在 `/etc/apache2/` 中，有兩個比較重要的 Config:

1. 有關整台 Apache，包括權限管理的設定檔，在 `apache2.conf` 中
2. httpd 的設定，virtual host 的設定，在 `sites-available` 中，再透過 Softlink 的方式連結到 `sites-enabled` 中

## Postgres 中 Setup 紀錄

- 開了一個 DB `testing-db`

## Server Environment Setup 紀錄

- 安裝 git
- 安裝 curl
- 安裝 Vim
- 安裝 Golang 1.16.2
  - `rm -rf /usr/local/go && tar -C /usr/local -xzf go1.16.2.linux-amd64.tar.gz`
  - 修改 `/etc/environment` 新增 `/usr/local/go/bin`
- 安裝 postgresql
  - `sudo apt install postgresql-12`
- 安裝 Apache

# TODO

- [x] Setup Ubuntu Development System (GO, Other Tools)
- [x] Setup Database Postgres (Testing Database on Machine)
- [x] Setup Apache
- [ ] Setup GIN
- [ ] Setup gorm
- [ ] Credential Setup
- [ ] Setup CORS
- [ ] Ubuntu UFW

## General

- [ ] Backend Testing Method
- [ ] GIT_LAB Deploy to Apache Server

##  System

- [ ] Fake Data
- [ ] Read API

## Authentication

- [ ] 查詢實做 Detail

## Document Upload System

- [ ] Static File System Implementation Survey
- [ ] Security Issue
- [ ] Cache Tuning