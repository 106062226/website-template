package router

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/cs-task-force/server/Api"
	"gitlab.com/cs-task-force/server/Middleware"
)

func Setup(engine *gin.Engine) {
	// Public API, JWT is not required in these routes.
	publicAPI := engine.Group("/api")

	// Activities routes

	publicAPI.GET("/sister/getAll", Api.GetSisters)

	publicAPI.GET("/activity/getAll", Api.GetActivities)
	publicAPI.GET("/target/getAll", Api.GetTargets)
	publicAPI.GET("/targetText/getAll", Api.GetTargetTexts)
	publicAPI.GET("/defaultInfo/getAll", Api.GetDefaultInfos)
	publicAPI.GET("/team/getAll", Api.GetTeams)
	publicAPI.GET("/award/getAll", Api.GetAwards)
	publicAPI.GET("/awardtag/getAll", Api.GetAwardtags)
	publicAPI.GET("/member/getAll", Api.GetMembers)
	publicAPI.GET("/activityRecord/getAll", Api.GetActivityRecords)
	publicAPI.GET("/meeting/getAll", Api.GetMeetings)
	publicAPI.GET("/book/getAll", Api.GetBooks)
	publicAPI.GET("/file/getAll", Api.GetPublicFiles)
	publicAPI.GET("/onlineLink/getAll", Api.GetOnlineSearchLinks)
	publicAPI.GET("/user/getAll", Api.GetUsers)

	// Dashboard API Group. Every requests passed to this group need
	// to check the presense of JWT token. Exception is signin api, which
	// doesn't require JWT token.
	managementPublicAPI := engine.Group("/management")
	managementAPI := engine.Group("/management")
	managementAPI.Use(Middleware.Token())

	// User routes.
	managementPublicAPI.POST("/user/signin", Api.UserSignin)
	managementAPI.GET("/user/check", Api.CheckJWTValid)
	managementAPI.POST("/user/create", Api.CreateUser)
	managementAPI.GET("/user/getAll", Api.GetUsers)
	managementAPI.POST("/user/update", Api.UpdateUser)
	managementAPI.POST("/user/delete", Api.DeleteUser)

	// Activities routes
	managementAPI.POST("/activity/create", Api.CreateActivity)
	managementAPI.GET("/activity/getAll", Api.GetActivities)
	managementAPI.POST("/activity/update", Api.UpdateActivity)
	managementAPI.POST("/activity/delete", Api.DeleteActivity)

	// Targets routes
	managementAPI.POST("/target/create", Api.CreateTarget)
	managementAPI.GET("/target/getAll", Api.GetTargets)
	managementAPI.POST("/target/update", Api.UpdateTarget)
	managementAPI.POST("/target/delete", Api.DeleteTarget)

	// TargetTexts routes
	managementAPI.POST("/targetText/create", Api.CreateTargetText)
	managementAPI.GET("/targetText/getAll", Api.GetTargetTexts)
	managementAPI.POST("/targetText/update", Api.UpdateTargetText)
	managementAPI.POST("/targetText/delete", Api.DeleteTargetText)

	// DefaultInfo routes
	managementAPI.POST("/defaultInfo/create", Api.CreateDefaultInfo)
	managementAPI.GET("/defaultInfo/getAll", Api.GetDefaultInfos)
	managementAPI.POST("/defaultInfo/update", Api.UpdateDefaultInfo)
	managementAPI.POST("/defaultInfo/delete", Api.DeleteDefaultInfo)

	// Teams routes
	managementAPI.POST("/team/create", Api.CreateTeam)
	managementAPI.GET("/team/getAll", Api.GetTeams)
	managementAPI.POST("/team/update", Api.UpdateTeam)
	managementAPI.POST("/team/delete", Api.DeleteTeam)

	// Award routes
	managementAPI.POST("/award/create", Api.CreateAward)
	managementAPI.GET("/award/getAll", Api.GetAwards)
	managementAPI.POST("/award/update", Api.UpdateAward)
	managementAPI.POST("/award/delete", Api.DeleteAward)

	// Awardtag routes
	managementAPI.POST("/awardtag/create", Api.CreateAwardtag)
	managementAPI.GET("/awardtag/getAll", Api.GetAwardtags)
	managementAPI.POST("/awardtag/update", Api.UpdateAwardtag)
	managementAPI.POST("/awardtag/delete", Api.DeleteAwardtag)

	// Members routes
	managementAPI.POST("/member/create", Api.CreateMember)
	managementAPI.GET("/member/getAll", Api.GetMembers)
	managementAPI.POST("/member/update", Api.UpdateMember)
	managementAPI.POST("/member/delete", Api.DeleteMember)

	// Activity Record
	managementAPI.POST("/activityRecord/create", Api.CreateActivityRecord)
	managementAPI.GET("/activityRecord/getAll", Api.GetActivityRecords)
	managementAPI.POST("/activityRecord/update", Api.UpdateActivityRecord)
	managementAPI.POST("/activityRecord/delete", Api.DeleteActivityRecord)

	// Meeting
	managementAPI.POST("/meeting/create", Api.CreateMeeting)
	managementAPI.GET("/meeting/getAll", Api.GetMeetings)
	managementAPI.POST("/meeting/update", Api.UpdateMeeting)
	managementAPI.POST("/meeting/delete", Api.DeleteMeeting)

	// Book
	managementAPI.POST("/book/create", Api.CreateBook)
	managementAPI.GET("/book/getAll", Api.GetBooks)
	managementAPI.POST("/book/update", Api.UpdateBook)
	managementAPI.POST("/book/delete", Api.DeleteBook)

	// File Upload
	managementAPI.POST("/file/upload", Api.FileUpload)
	managementAPI.GET("/file/getAll", Api.GetFiles)
	managementAPI.POST("/file/delete", Api.DeleteFile)
	managementAPI.POST("/file/updateType", Api.UpdateFileType)

	// Online Search Link
	managementAPI.POST("/onlineLink/create", Api.CreateOnlineSearchLink)
	managementAPI.GET("/onlineLink/getAll", Api.GetOnlineSearchLinks)
	managementAPI.POST("/onlineLink/update", Api.UpdateOnlineSearchLink)
	managementAPI.POST("/onlineLink/delete", Api.DeleteOnlineSearchLink)

	// Static File System
	engine.Static("/files", os.Getenv("STOREAGE_PATH"))
	engine.Static("/dashboard", os.Getenv("DASHBOARD_PATH"))
	engine.Static("/js", os.Getenv("DASHBOARD_PATH")+"/js")
	engine.Static("/css", os.Getenv("DASHBOARD_PATH")+"/css")
	engine.Static("/fonts", os.Getenv("DASHBOARD_PATH")+"/fonts")




	// Activities routes
	managementAPI.POST("/sister/create", Api.CreateSister)
	managementAPI.GET("/sister/getAll", Api.GetSisters)
	managementAPI.POST("/sister/update", Api.UpdateSister)
	managementAPI.POST("/sister/delete", Api.DeleteSister)
}
