package Auth

import (
	"crypto/rand"
	"errors"
	"log"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/cs-task-force/server/DB"
)

var JWT_TOKEN_SECRET []byte

// Claim Struct
type JWTCustomClaims struct {
	Name string `json:"name"`

	jwt.StandardClaims
}

func JWTCreateNewToken(user DB.User) (string, error) {

	// Create Claim
	claims := JWTCustomClaims{
		user.Name,
		jwt.StandardClaims{
			Issuer: "NTHU SERVER",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	result, err := token.SignedString(JWT_TOKEN_SECRET)
	if err != nil {
		return "", err
	}

	return result, nil
}

func JWTParseAndVerify(tokenString string) (jwt.MapClaims, error) {
	// Parse
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return JWT_TOKEN_SECRET, nil
	})

	if err != nil {
		return nil, err
	}

	// Verify
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	} else {
		return nil, errors.New("claims Not Found")
	}
}

func jwt_init() {
	JWT_TOKEN_SECRET = make([]byte, 128)

	_, err := rand.Read(JWT_TOKEN_SECRET)
	if err != nil {
		log.Fatalln("JWT Secret random failed.")
	}
}
