## Server Environment Setup 紀錄

- 安裝 git
- 安裝 curl
- 安裝 Vim
- 安裝 Golang 1.16.2
  - `rm -rf /usr/local/go && tar -C /usr/local -xzf go1.16.2.linux-amd64.tar.gz`
  - 修改 `/etc/environment` 新增 `/usr/local/go/bin`
- 安裝 postgresql
  - `sudo apt install postgresql-12`
  - 最後沒用到
- 安裝 Apache
- 安裝 nvm
  - 安裝 node 16.3.0
  - 安裝 `http-server` npm 模組到全域
- 創建 `~/Project` 資料夾
- Deploy 測試用前端伺服器
  - `http-server ~/Project/test-landing-page-server -p 8080`
- Deploy 測試用後端伺服器
  - port 9000
  - `~/Project/website-template/backend/script/start.sh`

## 提供的 Script

- `install.sh`：在 Project 根目錄創建 `build` 資料夾，裡面會包含 Backend 伺服器的執行檔，以及 Dashboard 編譯過後的原碼檔
  - `build/script/start.sh`：啟動伺服器
  - `build/script/user.py`：伺服器第一次啟動過後，可以透過此 Script 直接對 sqlite3 資料庫中的 user table 新增使用者
  - `build/script/key-generation.py`：Generate 密碼 hash 用的 key，可以自行修改產生長度

